# <img src="app/src/main/ic_launcher-playstore.png" alt="F-Droid Build Status" height="40"></img> F-Droid Build Status

Android App for the F-Droid build status API.

<a href="https://f-droid.org/en/packages/de.storchp.fdroidbuildstatus/">
                <img alt="Get it on F-Droid" src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="60" align="middle">
</a>


## Features

- Shows failed and successfull builds
- Shows build log in detail screen
- Shows published versions of an app
- Shows if an app needs an update or is disabled
- Mark apps as favourite for sorting
- Notify about new build status, optionally only of your favourite apps

## F-Droid API

The App uses the following F-Droid API endpoints:

- **running.json**: the current running F-Droid buildserver process, https://f-droid.org/repo/status/running.json
- **build.json**: the last completed F-Droid buildserver build cycle, https://f-droid.org/repo/status/build.json
- **update.json**: the last update check of the F-Droid buildserver, https://f-droid.org/repo/status/update.json
- **build.log**: `https://f-droid.org/repo/{id}_{versionCode}.log.gz`
- **published package info**: `https://f-droid.org/api/v1/packages/{id}`, example https://f-droid.org/api/v1/packages/org.fdroid.fdroid

## Screenshots

<div>
    <img width="25%" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1-mainscreen.jpg">
    <img width="25%" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2-builddetails.jpg">
    <img width="25%" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3-settings.jpg">
    <img width="25%" src="fastlane/metadata/android/en-US/images/phoneScreenshots/4-about.jpg">
</div>

## Legend

![currently running build cycle](artwork/directions_run-24px.svg) currently running build cycle (running.json)

![last finished build cycle](artwork/history-24px.svg) last finished build cycle (build.json)

![successful build](artwork/check-24px.svg) successful build

![failed build](artwork/error_outline-24px.svg) failed build, incomplete build cycle (maxBuildTimeReached)

![unknown build](artwork/help_outline-24px.svg) unknown build

![non-favourite app](artwork/star_border-24px.svg) non-favourite app

![favourite app](artwork/star-24px.svg) favourite app

![disabled app](artwork/disabled_by_default_24dp.svg) disabled app

![app needs update](artwork/upgrade_24dp.svg) app needs update


## Translations

This app is translated at: <a href="https://weblate.bubu1.eu/projects/f-droid_build_status/">Weblate.bubu1.eu</a>.

<a href="https://weblate.bubu1.eu/engage/f-droid_build_status/">
    <img src="https://weblate.bubu1.eu/widgets/f-droid_build_status/-/strings-xml/svg-badge.svg" alt="Translation state">
</a>

## Usefull information

- General F-Droid documentation page: https://f-droid.org/en/docs/
- F-Droid update processing: https://f-droid.org/en/docs/Update_Processing/
- https://gitlab.com/fdroid/wiki/-/wikis/FAQ#how-long-does-it-take-for-my-app-to-show-up-on-website-and-client

## Artwork

- Logo derived from F-Droid: https://gitlab.com/fdroid/artwork
- Icons from: https://github.com/material-components/material-components
- Acryl painting from @mondstern: https://pixelfed.social/p/mondstern/254646998685323264 (many thanks  :heart_eyes:)

## Related Projects

- Recent changes of the build process in F-Droid wiki: https://f-droid.org/wiki/index.php?title=Special:RecentChanges&hidebots=0&days=30&limit=500
- fdroid-build-checker: https://github.com/johnjohndoe/fdroid-build-checker
- F-Droid Monitor: https://monitor.f-droid.org/builds
