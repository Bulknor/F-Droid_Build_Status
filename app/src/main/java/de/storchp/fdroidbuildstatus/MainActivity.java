package de.storchp.fdroidbuildstatus;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.InputStreamReader;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import de.storchp.fdroidbuildstatus.api.FDroidAPI;
import de.storchp.fdroidbuildstatus.api.FDroidBuildRun;
import de.storchp.fdroidbuildstatus.api.Index;
import de.storchp.fdroidbuildstatus.api.Update;
import de.storchp.fdroidbuildstatus.databinding.ActivityMainBinding;
import de.storchp.fdroidbuildstatus.model.App;
import de.storchp.fdroidbuildstatus.model.BuildLoadType;
import de.storchp.fdroidbuildstatus.model.BuildRun;
import de.storchp.fdroidbuildstatus.model.BuildRunType;
import de.storchp.fdroidbuildstatus.monitor.MonitorJobService;
import de.storchp.fdroidbuildstatus.utils.DrawableUtils;
import de.storchp.fdroidbuildstatus.utils.FormatUtils;
import de.storchp.fdroidbuildstatus.utils.NotificationUtils;
import de.storchp.fdroidbuildstatus.utils.PreferenceUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static de.storchp.fdroidbuildstatus.utils.DrawableUtils.setMenuIconTint;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_BUILD_ITEM_ID = "EXTRA_BUILD_ITEM_ID";
    public static final String EXTRA_BUILD_ITEM_VERSION_CODE = "EXTRA_BUILD_ITEM_VERSION_CODE";
    public static final String EXTRA_BUILD_ITEM_RUN_TYPE = "EXTRA_BUILD_ITEM_RUN_TYPE";
    public static final int REQUEST_CODE_DETAILS = 0;
    public static final int REQUEST_CODE_SETTINGS = 1;

    private static final String DATA_FILE_NAME = "index-v1.json";
    private static final String DIALOG_TAG = "APP_INFO";
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String F_DROID_ORG_PACKAGES = "://f-droid.org/packages/";
    private static final String LIST_INSTANCE_STATE = "LIST_INSTANCE_STATE";

    private ActivityMainBinding binding;
    private MainAppListAdapter adapter;
    private BaseApplication baseApplication;

    private final AtomicInteger runningFetches = new AtomicInteger(0);

    private String lastSearch;
    private Parcelable listInstanceState;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        baseApplication = (BaseApplication) getApplication();

        binding.buildList.setOnItemClickListener((parent, view, position, id) -> {
            final App item = adapter.getItemForPosition(position);
            final Intent detailIntent = new Intent( this, DetailActivity.class );
            detailIntent.putExtra(EXTRA_BUILD_ITEM_ID, item.getId());
            startActivityForResult(detailIntent, REQUEST_CODE_DETAILS);
        });

        binding.buildList.setOnItemLongClickListener((parent, view, position, id) -> {
            final App item = adapter.getItemForPosition(position);
            if (getString(R.string.not_found_build_item_name).equals(item.getName())) {
                new AlertDialog.Builder(MainActivity.this)
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle(R.string.app_name)
                        .setMessage(getApplicationContext().getString(R.string.add_not_found_app_as_favourite, item.getId()))
                        .setNeutralButton(R.string.ok, (dialog, which) -> toggleFavourite(item)).create().show();
            } else {
                toggleFavourite(item);
            }
            return true;
        });

        binding.buildStatusHeadFinished.setOnClickListener((buttonView) -> {
            final Set<BuildRunType> buildRunTypes = PreferenceUtils.getBuildLoadType(this).getBuildRunTypes();

            if (!buildRunTypes.contains(BuildRunType.FINISHED)) {
                changeBuildLoadType(BuildLoadType.MERGED);
            } else {
                if (buildRunTypes.contains(BuildRunType.RUNNING)) {
                    changeBuildLoadType(BuildLoadType.RUNNING);
                } else {
                    Toast.makeText(MainActivity.this, R.string.at_list_one_build_load_type, Toast.LENGTH_LONG).show();
                }
            }
        });

        binding.buildStatusHeadRunning.setOnClickListener((buttonView) -> {
            final Set<BuildRunType> buildRunTypes = PreferenceUtils.getBuildLoadType(this).getBuildRunTypes();
            if (!buildRunTypes.contains(BuildRunType.RUNNING)) {
                changeBuildLoadType(BuildLoadType.MERGED);
            } else if (buildRunTypes.contains(BuildRunType.FINISHED)) {
                changeBuildLoadType(BuildLoadType.FINISHED);
            } else {
                Toast.makeText(MainActivity.this, R.string.at_list_one_build_load_type, Toast.LENGTH_LONG).show();
            }
        });

        NotificationUtils.cancelNotification(this);

        if (getIntent() != null) {
            onNewIntent(getIntent());
        }

        loadUpdate();
        if (!PreferenceUtils.isRepoIndexLoaded(this)) {
            loadIndex();
        }

        if (savedInstanceState!=null) {
            listInstanceState = savedInstanceState.getParcelable(LIST_INSTANCE_STATE);
        }
    }

    private void loadUpdate() {
        final long lastUpdate = PreferenceUtils.getLastUpdateLoaded(this);
        if (System.currentTimeMillis() - lastUpdate < Constants.TIME.ONE_DAY) {
            return; // use cached data
        }

        startProgress();
        final Call<Update> updateCall = baseApplication.getFDroidAPI().getUpdate();
        updateCall.enqueue(new Callback<Update>() {
            @Override
            public void onResponse(@NotNull final Call<Update> call, @NotNull final Response<Update> response) {
                if (response.isSuccessful()) {
                    final Update update = response.body();
                    baseApplication.getDbAdapter().saveUpdate(update);
                    PreferenceUtils.setLastUpdateLoaded(MainActivity.this, System.currentTimeMillis());
                    reloadList( PreferenceUtils.getBuildLoadType(MainActivity.this), null);
                } else {
                    Log.e(TAG, "Error loading update: " + response.message());
                    Toast.makeText(getBaseContext(), getString(R.string.load_update_failed, response.message()), Toast.LENGTH_LONG).show();
                }
                stopProgress();
            }

            @Override
            public void onFailure(@NotNull final Call<Update> call, @NotNull final Throwable t) {
                Log.e(TAG, "Error loading update", t);
                stopProgress();
                Toast.makeText(getBaseContext(), getString(R.string.load_update_failed, t.getMessage()), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void loadIndex() {
        new AlertDialog.Builder(MainActivity.this)
                .setIcon(R.mipmap.ic_launcher)
                .setTitle(R.string.app_name)
                .setMessage(R.string.load_index_question)
                .setNegativeButton(R.string.not_now, ((dialog, which) -> PreferenceUtils.setRepoIndexLoaded(MainActivity.this)))
                .setNeutralButton(R.string.download, (dialog, which) -> fetchIndex()).create().show();
    }

    private void fetchIndex() {
        startProgress();
        final Call<ResponseBody> indexCall = baseApplication.getFDroidAPI().getIndex();
        indexCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull final Call<ResponseBody> call, @NotNull final Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try (final JarInputStream jarFile = new JarInputStream(response.body().byteStream(), true)) {
                        JarEntry indexEntry = null;
                        JarEntry entry = jarFile.getNextJarEntry();
                        do {
                            if (entry.getName().equals(DATA_FILE_NAME)) {
                                indexEntry = entry;
                            } else {
                                entry = jarFile.getNextJarEntry();
                            }
                        } while (indexEntry == null && entry != null);

                        if (indexEntry != null) {
                            final Index index = new GsonBuilder().create().fromJson(new InputStreamReader(jarFile), Index.class);
                            baseApplication.getDbAdapter().updateApps(index.getApps());
                            PreferenceUtils.setRepoIndexLoaded(MainActivity.this);
                            refreshList();
                            Toast.makeText(getBaseContext(), getString(R.string.load_index_success, index.getApps().size()), Toast.LENGTH_LONG).show();
                        }
                    } catch (final Exception e) {
                        Log.e(TAG, "Error loading repo index", e);
                        Toast.makeText(getBaseContext(), getString(R.string.load_index_failed, e.getMessage()), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.e(TAG, "Error loading repo index: " + response.message());
                    Toast.makeText(getBaseContext(), getString(R.string.load_index_failed, response.message()), Toast.LENGTH_LONG).show();
                }
                stopProgress();
            }

            @Override
            public void onFailure(@NotNull final Call<ResponseBody> call, @NotNull final Throwable t) {
                Log.e(TAG, "Error loading repo index", t);
                stopProgress();
                Toast.makeText(getBaseContext(), getString(R.string.load_index_failed, t.getMessage()), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList();
        if (listInstanceState != null) {
            binding.buildList.onRestoreInstanceState(listInstanceState);
        }
        initBuildLoadTypeButtons(PreferenceUtils.getBuildLoadType(this));
    }

    private void changeBuildLoadType(final BuildLoadType buildLoadType) {
        PreferenceUtils.setBuildLoadType(this, buildLoadType);
        initBuildLoadTypeButtons(buildLoadType);
        loadData(buildLoadType, false);
    }

    private void initBuildLoadTypeButtons(final BuildLoadType buildLoadType) {
        final Set<BuildRunType> buildRunTypes = buildLoadType.getBuildRunTypes();
        binding.buildStatusHeadFinished.setBackgroundColor(buildRunTypes.contains(BuildRunType.FINISHED) ? getResources().getColor(R.color.colorPrimary) : getResources().getColor(R.color.disabled));
        binding.buildStatusHeadRunning.setBackgroundColor(buildRunTypes.contains(BuildRunType.RUNNING) ? getResources().getColor(R.color.colorPrimary) : getResources().getColor(R.color.disabled));
    }

    private void toggleFavourite(final App item) {
        item.setFavourite(!item.isFavourite());
        baseApplication.getDbAdapter().toggleFavourite(item.getId());
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            lastSearch = intent.getStringExtra(SearchManager.QUERY);
        } else if (Intent.ACTION_SEND.equals(intent.getAction())) {
            final String text = intent.getStringExtra(Intent.EXTRA_TEXT);
            if (text != null) {
                final int pos = text.indexOf(F_DROID_ORG_PACKAGES);
                if (pos > 0) {
                    lastSearch = text.substring(pos + F_DROID_ORG_PACKAGES.length());
                }
            }
        } else if (intent.getData() != null && Intent.ACTION_VIEW.equals(intent.getAction())) {
            final Uri data = intent.getData();
            final String scheme = data.getScheme();
            final String path = data.getPath();
            String packageName = null;
            if (data.isHierarchical()) {
                final String host = data.getHost();
                if (host == null) {
                    return;
                }
                switch (host) {
                    case "f-droid.org":
                    case "www.f-droid.org":
                    case "staging.f-droid.org":
                        if (path.startsWith("/app/") || path.startsWith("/packages/")
                                || path.matches("^/[a-z][a-z][a-zA-Z_-]*/packages/.*")) {
                            // http://f-droid.org/app/packageName
                            packageName = data.getLastPathSegment();
                        }
                        break;
                    case "details":
                        // market://details?id=app.id
                        packageName = data.getQueryParameter("id");
                        break;
                }
            } else if ("fdroid.app".equals(scheme)) {
                // fdroid.app:app.id
                packageName = data.getSchemeSpecificPart();
            }

            if (!TextUtils.isEmpty(packageName)) {
                Log.d(TAG, "launched via app link for '" + packageName + "'");
                lastSearch = packageName;
            }
        }
    }

    private void loadData(final BuildLoadType buildLoadType, final boolean forceUpdate) {
        final Map<BuildRunType, BuildRun> buildRuns = baseApplication.getDbAdapter().loadBuildRuns( buildLoadType.getBuildRunTypes());
        reloadList(buildLoadType, buildRuns);

        for (final BuildRunType buildRunType : buildLoadType.getBuildRunTypes()) {
            if (buildRunType.isUpdateable()) {
                final BuildRun buildRun = buildRuns.get(buildRunType);
                if (forceUpdate || buildRun == null || buildRun.needsUpdate()) {
                    fetchData(buildRunType);
                }
            }
        }
    }

    private void fetchData(final BuildRunType buildRunType) {
        final Call<FDroidBuildRun> buildStatusCall = buildRunType.getBuildRun(baseApplication.getFDroidAPI());
        startProgress();

        buildStatusCall.enqueue(new Callback<FDroidBuildRun>() {
            @Override
            public void onResponse(@NotNull final Call<FDroidBuildRun> call, @NotNull final Response<FDroidBuildRun> response) {
                final FDroidBuildRun buildRun;
                if (response.isSuccessful()) {
                    buildRun = response.body();
                    buildRun.setLastModified(FDroidAPI.getLastModified(response));
                    buildRun.setLastUpdated(new Date());
                    buildRun.setBuildRunType(buildRunType);
                    baseApplication.getDbAdapter().saveBuildRun(buildRun);
                    reloadList( PreferenceUtils.getBuildLoadType(MainActivity.this), null);
                    stopProgress();
                }
            }

            @Override
            public void onFailure(@NotNull final Call<FDroidBuildRun> call, @NotNull final Throwable t) {
                Log.e(TAG, "Error loading build status", t);
                stopProgress();
                Toast.makeText(getBaseContext(), getString(R.string.update_failed, t.getMessage()), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void reloadList(final BuildLoadType buildLoadType, final Map<BuildRunType, BuildRun> cachedBuildRuns) {
        setBuildStatusText(cachedBuildRuns != null ? cachedBuildRuns : baseApplication.getDbAdapter().loadBuildRuns(buildLoadType.getBuildRunTypes()));

        adapter = new MainAppListAdapter(MainActivity.this, baseApplication.getDbAdapter(), buildLoadType.getBuildRunTypes());
        binding.buildList.setAdapter(adapter);

        if (!TextUtils.isEmpty(lastSearch)) {
            searchList(lastSearch);
        }
    }

    private void startProgress() {
        runningFetches.incrementAndGet();
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    private void stopProgress() {
        if (runningFetches.decrementAndGet() == 0) {
            binding.progressBar.setVisibility(View.GONE);
        }
    }

    private void setBuildStatusText(final Map<BuildRunType, BuildRun> buildRuns) {
        final BuildRun buildRunRunning = buildRuns.get(BuildRunType.RUNNING);
        if (buildRunRunning != null) {
            if (!"build".equals(buildRunRunning.getSubcommand())) {
                binding.buildStatusHeadRunning.setText(getString(R.string.build_status_head_running_with_subcommand, buildRunRunning.getSubcommand()));
            } else {
                binding.buildStatusHeadRunning.setText(R.string.build_status_head_running);
            }
            binding.buildStatusStartRunning.setText(FormatUtils.formatShortDateTime(buildRunRunning.getStartTimestamp()));
            binding.buildStatusEndRunning.setText(buildRunRunning.getEndTimestamp() > 0 ? FormatUtils.formatShortDateTime(buildRunRunning.getEndTimestamp()) : "");
            binding.buildStatusLastModifiedRunning.setText(FormatUtils.formatShortDateTime(buildRunRunning.getLastModified().getTime()));
            binding.buildStatusSuccessfulRunning.setText(String.valueOf(buildRunRunning.getSuccessCount()));
            binding.buildStatusFailedRunning.setText(String.valueOf(buildRunRunning.getFailedCount()));
        } else {
            binding.buildStatusHeadRunning.setText(R.string.build_status_head_running);
            binding.buildStatusStartRunning.setText("");
            binding.buildStatusEndRunning.setText("");
            binding.buildStatusLastModifiedRunning.setText("");
            binding.buildStatusSuccessfulRunning.setText("");
            binding.buildStatusFailedRunning.setText("");
        }
        final BuildRun buildRunFinished = buildRuns.get(BuildRunType.FINISHED);
        if (buildRunFinished != null) {
            binding.buildStatusStartFinished.setText(FormatUtils.formatShortDateTime(buildRunFinished.getStartTimestamp()));
            binding.buildStatusEndFinished.setText(FormatUtils.formatShortDateTime(buildRunFinished.getEndTimestamp()));
            binding.buildStatusLastModifiedFinished.setText(FormatUtils.formatShortDateTime(buildRunFinished.getLastModified().getTime()));
            binding.buildStatusSuccessfulFinished.setText(String.valueOf(buildRunFinished.getSuccessCount()));
            binding.buildStatusFailedFinished.setText(String.valueOf(buildRunFinished.getFailedCount()));
            if (buildRunFinished.isMaxBuildTimeReached()) {
                DrawableUtils.setCompoundDrawablesRight(this, binding.buildStatusEndFinished, R.drawable.ic_error_outline_24px, binding.buildStatusEnd.getCurrentTextColor());
                binding.buildStatusEndFinished.setCompoundDrawablePadding(5);
            } else {
                binding.buildStatusEndFinished.setCompoundDrawables(null, null, null, null);
            }
        } else {
            binding.buildStatusStartFinished.setText("");
            binding.buildStatusEndFinished.setText("");
            binding.buildStatusEndFinished.setCompoundDrawables(null, null, null, null);
            binding.buildStatusLastModifiedFinished.setText("");
            binding.buildStatusSuccessfulFinished.setText("");
            binding.buildStatusFailedFinished.setText("");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        setMenuIconTint(this, menu, R.id.action_search, R.color.design_default_color_on_primary);
        setMenuIconTint(this, menu, R.id.action_refresh, R.color.design_default_color_on_primary);

        final SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchMenu = menu.findItem(R.id.action_search);
        searchMenu.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(final MenuItem arg0) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(final MenuItem arg0) {
                // fix #17
                invalidateOptionsMenu();
                return true;
            }
        });

        final SearchView searchView = (SearchView) searchMenu.getActionView();
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String s) {
                Log.d(TAG, "onQueryTextSubmit: " + s);
                searchList(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String s) {
                Log.d(TAG, "onQueryTextChange: " + s);
                searchList(s);
                return false;
            }

        });

        return true;
    }

    private void searchList(final String search) {
        this.lastSearch = search;
        adapter.getFilter().filter(search);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivityForResult(new Intent(this, SettingsActivity.class), REQUEST_CODE_SETTINGS);
            return true;
        } else if (id == R.id.action_refresh) {
            loadData( PreferenceUtils.getBuildLoadType(this), true);
            return true;
        } else if (id == R.id.action_about) {
            final AppInfoFragment appInfoFragment = new AppInfoFragment();
            appInfoFragment.show(getSupportFragmentManager(), DIALOG_TAG);
            return true;
        } else if (id == R.id.action_load_index) {
            loadIndex();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_DETAILS) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        } else if (requestCode == REQUEST_CODE_SETTINGS) {
            refreshList();
            // update monitoring service after setting changes
            MonitorJobService.schedule(this);
        }
    }

    private void refreshList() {
        final BuildLoadType buildLoadType = PreferenceUtils.getBuildLoadType(this);
        initBuildLoadTypeButtons(buildLoadType);
        loadData(buildLoadType, false);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(LIST_INSTANCE_STATE, binding.buildList.onSaveInstanceState());
    }

    @Override
    protected void onPause() {
        super.onPause();
        listInstanceState = binding.buildList.onSaveInstanceState();
    }

}