package de.storchp.fdroidbuildstatus.model;

import java.util.Date;

import de.storchp.fdroidbuildstatus.Constants;
import de.storchp.fdroidbuildstatus.R;
import de.storchp.fdroidbuildstatus.api.FDroidAPI;
import de.storchp.fdroidbuildstatus.api.FDroidBuildRun;
import retrofit2.Call;

public enum BuildRunType {

    RUNNING(true, R.drawable.ic_directions_run_24px) {
        @Override
        public Call<FDroidBuildRun> getBuildRun(final FDroidAPI api) {
            return api.getRunningBuildRun();
        }

        @Override
        public boolean needsUpdate(final long lastModifiedTime, final Date lastUpdated) {
            return System.currentTimeMillis() - lastModifiedTime > Constants.TIME.ONE_HOUR
                    && lastUpdateExpired(lastUpdated);
        }

    },
    FINISHED(true, R.drawable.ic_history_24px) {
        @Override
        public Call<FDroidBuildRun> getBuildRun(final FDroidAPI api) {
            return api.getFinishedBuildRun();
        }

        @Override
        public boolean needsUpdate(final long lastModifiedTime, final Date lastUpdated) {
            return System.currentTimeMillis() - lastModifiedTime > Constants.TIME.ONE_DAY
                    && lastUpdateExpired(lastUpdated);
        }
    },
    NONE(false, 0) {
        @Override
        public Call<FDroidBuildRun> getBuildRun(final FDroidAPI api) {
            throw new IllegalStateException("");
        }

        @Override
        public boolean needsUpdate(final long lastModifiedTime, final Date lastUpdated) {
            return false;
        }
    };

    private final boolean updateable;
    private final int iconRes;

    BuildRunType(final boolean updateable, final int iconRes) {
        this.updateable = updateable;
        this.iconRes = iconRes;
    }

    public abstract Call<FDroidBuildRun> getBuildRun(final FDroidAPI api);

    public abstract boolean needsUpdate(final long lastModifiedTime, final Date lastUpdated);

    private static boolean lastUpdateExpired(final Date lastUpdated) {
        return lastUpdated == null || System.currentTimeMillis() - lastUpdated.getTime() > Constants.TIME.ONE_HOUR;
    }

    public boolean isUpdateable() {
        return updateable;
    }

    public int getIconRes() {
        return iconRes;
    }
}
