package de.storchp.fdroidbuildstatus.model;

import android.text.TextUtils;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import de.storchp.fdroidbuildstatus.Constants;
import de.storchp.fdroidbuildstatus.R;

public class App {

    private String id;
    private String name;
    private boolean favourite;
    private String sourceCode;
    private boolean disabled = false;
    private boolean needsUpdate = false;

    // contains all builds of an app, lazy loaded
    private final Set<AppBuild> appBuilds = new HashSet<>();

    public App() {
    }

    public App(final String id, final String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(final boolean favourite) {
        this.favourite = favourite;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof App)) return false;
        final App app = (App) o;
        return id.equals(app.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Returns the name if given otherwise the id
     */
    public String getDisplayName() {
        if (TextUtils.isEmpty(name)) {
            return id;
        }
        return name;
    }

    public int getFavouriteIcon() {
        return isFavourite() ? R.drawable.ic_star_24px : R.drawable.ic_star_border_24px;
    }

    public String getFdroidUri() {
        return Constants.FDROID_PACKAGES_URI + getId();
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(final String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public void setDisabled(final boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setNeedsUpdate(final boolean needsUpdate) {
        this.needsUpdate = needsUpdate;
    }

    public boolean isNeedsUpdate() {
        return needsUpdate;
    }

    public Set<AppBuild> getAppBuilds() {
        return appBuilds;
    }

    public String getMetadataUri() {
        return Constants.FDROIDDATA_BLOB_URI + "master/metadata/" + getId() + ".yml";
    }

}
