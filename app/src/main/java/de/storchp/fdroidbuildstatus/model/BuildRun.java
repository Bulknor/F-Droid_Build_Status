package de.storchp.fdroidbuildstatus.model;

import java.util.Date;

public class BuildRun {

    private long endTimestamp = 0;
    private long startTimestamp = 0;
    private Date lastModified = new Date();
    private BuildRunType buildRunType = BuildRunType.NONE;
    private Date lastUpdated = new Date();
    private boolean maxBuildTimeReached = false;
    private String subcommand;
    private int successCount = 0;
    private int failedCount = 0;
    private String commitId;

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(final long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(final long startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public void setLastModified(final Date lastModified) {
        this.lastModified = lastModified;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setBuildRunType(final BuildRunType buildRunType) {
        this.buildRunType = buildRunType;
    }

    public BuildRunType getBuildRunType() {
        return buildRunType;
    }

    public void setLastUpdated(final Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public boolean needsUpdate() {
        return buildRunType.needsUpdate(lastModified.getTime(), lastUpdated);
    }

    public boolean isMaxBuildTimeReached() {
        return maxBuildTimeReached;
    }

    public void setMaxBuildTimeReached(final boolean maxBuildTimeReached) {
        this.maxBuildTimeReached = maxBuildTimeReached;
    }

    public String getSubcommand() {
        return subcommand;
    }

    public void setSubcommand(final String subcommand) {
        this.subcommand = subcommand;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(final int successCount) {
        this.successCount = successCount;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(final int failedCount) {
        this.failedCount = failedCount;
    }

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(final String commitId) {
        this.commitId = commitId;
    }

}
