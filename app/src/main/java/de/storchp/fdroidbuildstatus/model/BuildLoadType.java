package de.storchp.fdroidbuildstatus.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum BuildLoadType {

    FINISHED(BuildRunType.FINISHED),
    RUNNING(BuildRunType.RUNNING),
    MERGED(BuildRunType.FINISHED, BuildRunType.RUNNING);

    private final BuildRunType[] buildRunTypes;

    BuildLoadType(final BuildRunType ... buildRunTypes) {
        this.buildRunTypes = buildRunTypes;
    }

    public Set<BuildRunType> getBuildRunTypes() {
        return new HashSet<>(Arrays.asList(buildRunTypes));
    }

}
