package de.storchp.fdroidbuildstatus.model;

import java.util.Objects;

public class AppNotification {

    private String id;

    private String versionCode;

    public AppNotification(final String id, final String versionCode) {
        this.id = id;
        this.versionCode = versionCode;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(final String versionCode) {
        this.versionCode = versionCode;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final AppNotification that = (AppNotification) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(versionCode, that.versionCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, versionCode);
    }
}
