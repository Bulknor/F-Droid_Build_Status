package de.storchp.fdroidbuildstatus.model;

import de.storchp.fdroidbuildstatus.R;

public enum BuildStatus {
    UNKNOWN(R.drawable.ic_questionmark_outline_24px, R.string.unknown_build_version),
    FAILED(R.drawable.ic_error_outline_24px, R.string.failed_build_version),
    SUCCESS(R.drawable.ic_check_24px, R.string.success_build_version);

    private final int iconRes;
    private final int versionPrefixResId;

    BuildStatus(final int iconRes, final int versionPrefixResId) {
        this.iconRes = iconRes;
        this.versionPrefixResId = versionPrefixResId;
    }

    public int getIconRes() {
        return iconRes;
    }

    public int getVersionPrefixResId() {
        return versionPrefixResId;
    }

}
