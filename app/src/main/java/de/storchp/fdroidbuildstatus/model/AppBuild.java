package de.storchp.fdroidbuildstatus.model;

import android.content.Context;

import java.util.Objects;

import de.storchp.fdroidbuildstatus.Constants;
import de.storchp.fdroidbuildstatus.utils.FormatUtils;

public class AppBuild implements Comparable<AppBuild> {

    private String version;
    private String versionCode;
    private BuildStatus status = BuildStatus.UNKNOWN;
    private BuildRunType buildRunType = BuildRunType.NONE;
    private String metadatapath;
    private String dataCommitId;

    public AppBuild() {
        super();
    }

    public String getFullVersion(final Context context) {
        return context.getString(status.getVersionPrefixResId())
                + FormatUtils.formatVersion(versionCode, version);
    }

    public BuildStatus getStatus() {
        return status;
    }

    public void setStatus(final BuildStatus status) {
        this.status = status;
    }

    public BuildRunType getBuildRunType() {
        return buildRunType;
    }

    public void setBuildRunType(final BuildRunType buildRunType) {
        this.buildRunType = buildRunType;
    }

    public void setDataCommitId(final String dataCommitId) {
        this.dataCommitId = dataCommitId;
    }

    public String getMetadatapath() {
        return metadatapath;
    }

    public void setMetadatapath(final String metadatapath) {
        this.metadatapath = metadatapath;
    }

    public String getMetadataUri(final String id) {
        String metadataUri = Constants.FDROIDDATA_BLOB_URI;
        if (dataCommitId == null) {
            metadataUri += "master";
        } else {
            metadataUri += dataCommitId;
        }
        if (metadatapath == null) {
            metadataUri += "/metadata/" + id + ".yml";
        } else {
            metadataUri += metadatapath;
        }
        return metadataUri;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(final String versionCode) {
        this.versionCode = versionCode;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final AppBuild appBuild = (AppBuild) o;
        return versionCode.equals(appBuild.versionCode) &&
                buildRunType == appBuild.buildRunType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(versionCode, buildRunType);
    }

    @Override
    public int compareTo(final AppBuild o) {
        final int buildRunTypeCompare = buildRunType.compareTo(o.getBuildRunType());
        if (buildRunTypeCompare != 0) {
            return buildRunTypeCompare;
        }

        return -Integer.compare(Integer.parseInt(versionCode), Integer.parseInt(o.getVersionCode()));
    }

}
