package de.storchp.fdroidbuildstatus;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import de.storchp.fdroidbuildstatus.api.Build;
import de.storchp.fdroidbuildstatus.api.GitlabAPI;
import de.storchp.fdroidbuildstatus.api.Metadata;
import de.storchp.fdroidbuildstatus.api.PublishedVersions;
import de.storchp.fdroidbuildstatus.databinding.ActivityDetailBinding;
import de.storchp.fdroidbuildstatus.model.App;
import de.storchp.fdroidbuildstatus.model.AppBuild;
import de.storchp.fdroidbuildstatus.model.BuildRunType;
import de.storchp.fdroidbuildstatus.utils.DrawableUtils;
import de.storchp.fdroidbuildstatus.utils.FormatUtils;
import de.storchp.fdroidbuildstatus.utils.PreferenceUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static de.storchp.fdroidbuildstatus.utils.DrawableUtils.setMenuIconTint;

public class DetailActivity extends AppCompatActivity {

    private static final String TAG = DetailActivity.class.getSimpleName();

    private ActivityDetailBinding binding;
    private App app;
    private BaseApplication baseApplication;
    private final AtomicInteger runningFetches = new AtomicInteger(0);

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        baseApplication = (BaseApplication) getApplication();
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);

        if (intent != null) {
            final String id = intent.getStringExtra(MainActivity.EXTRA_BUILD_ITEM_ID);
            final String versionCode = intent.getStringExtra(MainActivity.EXTRA_BUILD_ITEM_VERSION_CODE);
            final String buildRunType = intent.getStringExtra(MainActivity.EXTRA_BUILD_ITEM_RUN_TYPE);
            app = baseApplication.getDbAdapter().loadAppBuilds(id, PreferenceUtils.getBuildLoadType(this).getBuildRunTypes());

            if (app == null) {
                app = baseApplication.getDbAdapter().loadApp(id);
                if (app == null) {
                    app = new App(id, getString(R.string.not_found_build_item_name));
                }
                app.getAppBuilds().add(new AppBuild());
            }

            binding.appName.setText(app.getName());
            binding.appId.setText(app.getId());

            DrawableUtils.setIconWithTint(this, binding.disabledIcon, R.drawable.ic_disabled_by_default_black_24dp, binding.appName.getCurrentTextColor());
            DrawableUtils.setIconWithTint(this, binding.needsUpdateIcon, R.drawable.ic_upgrade_black_24dp, binding.appName.getCurrentTextColor());
            DrawableUtils.setIconWithTint(this, binding.favouriteIcon, app.getFavouriteIcon(), binding.appName.getCurrentTextColor());

            binding.disabledIcon.setVisibility(app.isDisabled() ? View.VISIBLE : View.GONE);
            binding.needsUpdateIcon.setVisibility(app.isNeedsUpdate() ? View.VISIBLE : View.GONE);

            final DetailAppBuildListAdapter detailAppBuildListAdapter = new DetailAppBuildListAdapter(this, app.getAppBuilds());
            binding.builds.setAdapter(detailAppBuildListAdapter);
            int selectedPos = 0;
            if (versionCode != null) {
                selectedPos = detailAppBuildListAdapter.findPosBy(versionCode, BuildRunType.valueOf(buildRunType));
            }
            binding.builds.setItemChecked(selectedPos, true);
            loadAndSetBuildLog(detailAppBuildListAdapter.getItemForPosition(selectedPos));

            binding.builds.setOnItemClickListener((parent, view, position, itemId) -> {
                final AppBuild appBuild = detailAppBuildListAdapter.getItemForPosition(position);
                loadAndSetBuildLog(appBuild);
            });

            fetchPublishedVersions(app.getId());
            fetchMetadataVersion(app.getId());
        }
    }

    private void fetchMetadataVersion(final String id) {
        startProgress();
        final Call<Metadata> call = GitlabAPI.create().getFdroidDataMetadata(id);

        call.enqueue(new Callback<Metadata>() {
            @Override
            public void onResponse(@NotNull final Call<Metadata> call, @NotNull final Response<Metadata> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "loaded metadata versions");
                    final Metadata metadata = response.body();
                    if (metadata != null) {
                        final Build highestVersion = metadata.getHighestVersion();
                        binding.metadataVersion.setText(getString(R.string.metadata_version, FormatUtils.formatVersion(highestVersion.getVersionCode(), highestVersion.getVersionName())));
                    }
                } else if (response.code() == 404) {
                    binding.metadataVersion.setText(R.string.metadata_version_none);
                } else {
                    Log.d(TAG, "server contact failed");
                    Toast.makeText(DetailActivity.this, getResources().getString(R.string.loading_metadata_versions_failed, response), Toast.LENGTH_LONG).show();
                }
                stopProgress();
            }

            @Override
            public void onFailure(@NotNull final Call<Metadata> call, @NotNull final Throwable t) {
                Log.e(TAG, "error", t);
                Toast.makeText(DetailActivity.this, getResources().getString(R.string.loading_metadata_versions_failed, t.getMessage()), Toast.LENGTH_LONG).show();
                stopProgress();
            }
        });
    }

    private void loadAndSetBuildLog(final AppBuild appBuild) {
        if (appBuild.getBuildRunType().isUpdateable()) {
            final String log = baseApplication.getDbAdapter().getBuildLog(app.getId(), appBuild.getVersionCode(), appBuild.getBuildRunType());
            if (log != null) {
                setBuildLog(log, app.getId(), appBuild.getVersionCode(), appBuild.getBuildRunType());
            } else {
                fetchBuildLog(app.getId(), appBuild.getVersionCode(), appBuild.getBuildRunType());
            }
        }
    }

    private void fetchPublishedVersions(final String id) {
        startProgress();
        final Call<PublishedVersions> call = baseApplication.getFDroidAPI().getPublishedVersions(id);

        call.enqueue(new Callback<PublishedVersions>() {
            @Override
            public void onResponse(@NotNull final Call<PublishedVersions> call, @NotNull final Response<PublishedVersions> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "loaded published versions");
                    final PublishedVersions publishedVersions = response.body();
                    if (publishedVersions != null) {
                        final StringBuilder versions = new StringBuilder(getResources().getString(R.string.published_versions));
                        for (final PublishedVersions.PublishedPackage version : publishedVersions.getPackages()) {
                            versions.append("\n\u00B7 ")
                                    .append(FormatUtils.formatVersion(version.getVersionCode(), version.getVersionName()));
                        }
                        binding.publishedVersions.setText(versions);
                    }
                } else if (response.code() == 404) {
                    binding.publishedVersions.setText(R.string.published_versions_none);
                } else {
                    Log.d(TAG, "server contact failed");
                    Toast.makeText(DetailActivity.this, getResources().getString(R.string.loading_published_versions_failed, response), Toast.LENGTH_LONG).show();
                }
                stopProgress();
            }

            @Override
            public void onFailure(@NotNull final Call<PublishedVersions> call, @NotNull final Throwable t) {
                Log.e(TAG, "error", t);
                Toast.makeText(DetailActivity.this, getResources().getString(R.string.loading_published_versions_failed, t.getMessage()), Toast.LENGTH_LONG).show();
                stopProgress();
            }
        });
    }

    private void fetchBuildLog(final String id, final String versionCode, final BuildRunType buildRunType) {
        startProgress();
        final Call<ResponseBody> call = baseApplication.getFDroidAPI().getBuildLog(id, versionCode);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull final Call<ResponseBody> call, @NotNull final Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "loaded logfile");
                    try {
                        final ResponseBody body = response.body();
                        if (body != null) {
                            final String log = body.string();
                            setBuildLog(log, null, null, buildRunType);
                            baseApplication.getDbAdapter().updateBuildLog(id, versionCode, buildRunType, log);
                        }
                    } catch (final IOException e) {
                        Log.d(TAG, "get response body failed", e);
                    }
                } else {
                    Log.d(TAG, "server contact failed");
                    Toast.makeText(DetailActivity.this, getResources().getString(R.string.loading_build_log_failed, response), Toast.LENGTH_LONG).show();
                }
                stopProgress();
            }

            @Override
            public void onFailure(@NotNull final Call<ResponseBody> call, @NotNull final Throwable t) {
                Log.e(TAG, "error", t);
                Toast.makeText(DetailActivity.this, getResources().getString(R.string.loading_build_log_failed, t.getMessage()), Toast.LENGTH_LONG).show();
                stopProgress();
            }
        });
    }

    private void setBuildLog(final String log, final String id, final String versionCode, final BuildRunType buildRunType) {
        Log.d(TAG, "Log.size: " + log.length());
        final String[] lines = log.split("\n");
        binding.buildLog.setAdapter(new ArrayAdapter<>(this, R.layout.item_logline, lines));
        if (lines.length == 1 && id != null && versionCode != null) {
            // we might have an incomplete build log with only 1 line, fetch again
            fetchBuildLog(id, versionCode, buildRunType);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        setMenuIconTint(this, menu, R.id.action_open, R.color.design_default_color_on_primary);
        setMenuIconTint(this, menu, R.id.action_metadata, R.color.design_default_color_on_primary);
        setMenuIconTint(this, menu, R.id.action_source, R.color.design_default_color_on_primary);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_open) {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(app.getFdroidUri()));
            startActivity(intent);
            return true;
        } else if (id == R.id.action_metadata) {
            final AppBuild appBuild = (AppBuild) binding.builds.getSelectedItem();
            String metadataUri = app.getMetadataUri();
            if (appBuild != null) {
                metadataUri = appBuild.getMetadataUri(app.getId());
            }
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(metadataUri));
            startActivity(intent);
            return true;
        } else if (id == R.id.action_source) {
            try {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(app.getSourceCode()));
                startActivity(intent);
                return true;
            } catch (final Exception e) {
                Log.e(TAG, "Failed to start activity for SourceCode: " + app.getSourceCode());
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void toggleFavourite(final View view) {
        app.setFavourite(!app.isFavourite());
        baseApplication.getDbAdapter().toggleFavourite(app.getId());
        DrawableUtils.setIconWithTint(this, binding.favouriteIcon, app.getFavouriteIcon(), binding.appName.getCurrentTextColor());
    }

    private void startProgress() {
        runningFetches.incrementAndGet();
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    private void stopProgress() {
        if (runningFetches.decrementAndGet() == 0) {
            binding.progressBar.setVisibility(View.GONE);
        }
    }

}
