package de.storchp.fdroidbuildstatus.monitor;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;

import androidx.core.content.ContextCompat;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.storchp.fdroidbuildstatus.BaseApplication;
import de.storchp.fdroidbuildstatus.R;
import de.storchp.fdroidbuildstatus.api.BuildItem;
import de.storchp.fdroidbuildstatus.api.FDroidAPI;
import de.storchp.fdroidbuildstatus.api.FDroidBuildRun;
import de.storchp.fdroidbuildstatus.model.App;
import de.storchp.fdroidbuildstatus.model.AppNotification;
import de.storchp.fdroidbuildstatus.model.BuildRun;
import de.storchp.fdroidbuildstatus.model.BuildRunType;
import de.storchp.fdroidbuildstatus.utils.NotificationUtils;
import de.storchp.fdroidbuildstatus.utils.PreferenceUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION_CODES;

public class MonitorJobService extends JobService {

    private static final String TAG = MonitorJobService.class.getSimpleName();

    private static final int JOB_ID = 1000;

    public static void schedule(final Context context) {
        final boolean updateCheckEnabled = PreferenceUtils.isUpdateCheckEnabled(context);

        final JobScheduler jobScheduler = ContextCompat.getSystemService(context, JobScheduler.class);
        jobScheduler.cancel(JOB_ID); // cancel previous scheduled job

        if (!updateCheckEnabled) {
            Log.i(TAG, "MonitorService disabled");
            return;
        }

        final ComponentName serviceComponent = new ComponentName(context, MonitorJobService.class);
        final JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, serviceComponent);
        final long updateInterval = PreferenceUtils.getUpdateInterval(context);
        builder.setPeriodic(updateInterval)
               .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        if (VERSION.SDK_INT >= VERSION_CODES.O) {
            builder.setRequiresBatteryNotLow(true);
        }
        jobScheduler.schedule(builder.build());
        Log.i(TAG, "MonitorService scheduled for " + updateInterval + " millis interval");
    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        Log.i(TAG, "MonitorService job started");
        final BaseApplication baseApplication = (BaseApplication) getApplication();

        final BuildRunType buildRunType = PreferenceUtils.getBuildCheckType(this);
        final Call<FDroidBuildRun> buildStatusCall = buildRunType.getBuildRun(baseApplication.getFDroidAPI());

        buildStatusCall.enqueue(new Callback<FDroidBuildRun>() {
            @Override
            public void onResponse(@NotNull final Call<FDroidBuildRun> call, @NotNull final Response<FDroidBuildRun> response) {
                Log.d(TAG, "build status response: " + response.isSuccessful());
                if (response.isSuccessful()) {
                    final BuildRun oldBuildRun = baseApplication.getDbAdapter().loadBuildRuns(Collections.singleton(buildRunType)).get(buildRunType);
                    final FDroidBuildRun buildRun = response.body();
                    buildRun.setLastModified(FDroidAPI.getLastModified(response));
                    buildRun.setBuildRunType(buildRunType);
                    buildRun.setLastUpdated(new Date());
                    Log.d(TAG, "oldBuild Last-Modified: " + (oldBuildRun != null ? oldBuildRun.getLastModified() : "") + ", newBuildRun Last-Modified: " + buildRun.getLastModified());
                    if (oldBuildRun == null || !oldBuildRun.getLastModified().equals(buildRun.getLastModified())) {
                        Log.d(TAG, "New build detected " + buildRun.getLastModified());
                        baseApplication.getDbAdapter().saveBuildRun(buildRun);
                        final Map<String, App> favourites = baseApplication.getDbAdapter().getFavourites();
                        final boolean favouritesOnly = PreferenceUtils.isNotifyFavouritesOnly(MonitorJobService.this);
                        if (favouritesOnly) {
                            final Set<AppNotification> notifiedApps = baseApplication.getDbAdapter().getNotificationsFor(buildRunType, buildRun.getStartTimestamp());
                            final Set<AppNotification> newFavoriteApps = new HashSet<>();
                            for (final BuildItem item : buildRun.getAllBuilds()) {
                                final AppNotification appNotification = new AppNotification(item.getId(), item.getCurrentVersionCode());
                                if (favourites.containsKey(item.getId()) && !notifiedApps.contains(appNotification)) {
                                    newFavoriteApps.add(appNotification);
                                }
                            }
                            if (!newFavoriteApps.isEmpty()) {
                                if (newFavoriteApps.size() <= 2) {
                                    final StringBuilder names = new StringBuilder();
                                    for (final AppNotification appNotification : newFavoriteApps) {
                                        final App app = favourites.get(appNotification.getId());
                                        if (names.length() > 0) {
                                            names.append(", ");
                                        }
                                        names.append(app.getDisplayName());
                                    }
                                    NotificationUtils.createNewBuildNotification(MonitorJobService.this, getString(R.string.new_build_notification_fav_names, names.toString()));
                                } else {
                                    NotificationUtils.createNewBuildNotification(MonitorJobService.this, getString(R.string.new_build_notification_favs, newFavoriteApps.size()));
                                }
                                notifiedApps.addAll(newFavoriteApps);
                                baseApplication.getDbAdapter().saveNotifications(buildRunType, buildRun.getStartTimestamp(), notifiedApps);
                            }
                        } else {
                            NotificationUtils.createNewBuildNotification(MonitorJobService.this, getString(R.string.new_build_notification));
                        }
                    }
                } else {
                    Log.e(TAG, "MonitorService failed with " + response.errorBody());
                }
                jobFinished(params, false);
            }

            @Override
            public void onFailure(@NotNull final Call<FDroidBuildRun> call, @NotNull final Throwable t) {
                Log.e(TAG, "MonitorService failed", t);
                jobFinished(params, false);
            }
        });
        return true;
    }

    @Override
    public boolean onStopJob(final JobParameters params) {
        return true;
    }
}
