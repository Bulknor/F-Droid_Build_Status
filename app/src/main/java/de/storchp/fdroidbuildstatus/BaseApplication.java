package de.storchp.fdroidbuildstatus;

import android.app.Application;
import android.content.Context;

import de.storchp.fdroidbuildstatus.api.FDroidAPI;
import de.storchp.fdroidbuildstatus.monitor.MonitorJobService;
import de.storchp.fdroidbuildstatus.utils.NotificationUtils;

public class BaseApplication extends Application {

    private static final String TAG = BaseApplication.class.getSimpleName();
    private static final long ONE_HOUR = 1000 * 60 * 60;

    private FDroidAPI api;
    private DbAdapter dbAdapter;

    @Override
    protected void attachBaseContext(final Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        dbAdapter = new DbAdapter(this);
        dbAdapter.open();

        NotificationUtils.createNotificationChannel(this);
        MonitorJobService.schedule(this);
    }

    public DbAdapter getDbAdapter() {
        return dbAdapter;
    }

    public FDroidAPI getFDroidAPI() {
        if (api == null) {
            api = FDroidAPI.create();
        }
        return api;
    }

}
