package de.storchp.fdroidbuildstatus;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.util.Map;

import de.storchp.fdroidbuildstatus.model.BuildLoadType;
import de.storchp.fdroidbuildstatus.model.BuildRun;
import de.storchp.fdroidbuildstatus.model.BuildRunType;
import de.storchp.fdroidbuildstatus.utils.DrawableUtils;
import de.storchp.fdroidbuildstatus.utils.FormatUtils;

public class AppInfoFragment extends DialogFragment {

    @Override
    @NonNull
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        final TextView textView = new TextView(getContext());
        textView.setLinksClickable(true);
        textView.setTextSize((float) 16);
        textView.setPadding(40, 40, 40, 40);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        final Map<BuildRunType, BuildRun> buildRuns = ((BaseApplication)getActivity().getApplication()).getDbAdapter().loadBuildRuns(BuildLoadType.MERGED.getBuildRunTypes());
        final String lastUpdateBuild = getLastUpdate(buildRuns, BuildRunType.FINISHED);
        final String lastUpdateRunning = getLastUpdate(buildRuns, BuildRunType.RUNNING);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(getString(R.string.app_info_text, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE, lastUpdateBuild, lastUpdateRunning), Html.FROM_HTML_MODE_COMPACT, new ImageGetter(getContext(), textView.getCurrentTextColor()), null));
        } else {
            textView.setText(Html.fromHtml(getString(R.string.app_info_text, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE, lastUpdateBuild, lastUpdateRunning), new ImageGetter(getContext(), textView.getCurrentTextColor()), null));
        }

        builder.setIcon(R.mipmap.ic_launcher)
                .setTitle(R.string.app_info_title)
                .setPositiveButton(R.string.ok, null);

        builder.setView(textView);

        return builder.create();
    }

    @NotNull
    private String getLastUpdate(final Map<BuildRunType, BuildRun> buildRuns, final BuildRunType runType) {
        return buildRuns.containsKey(runType) ? FormatUtils.formatShortDateTime(buildRuns.get(runType).getLastUpdated().getTime()) : "";
    }

    private class ImageGetter implements Html.ImageGetter {

        private final int textColor;
        private final WeakReference<Context> contextRef;

        public ImageGetter(final Context context, final int textColor) {
            this.contextRef = new WeakReference<>(context);
            this.textColor = textColor;
        }

        public Drawable getDrawable(String source) {
            final Context context = (Context) contextRef.get();
            if (context == null) {
                return null;
            }
            if (source.endsWith("/")) {
                source = source.substring(0, source.length() -1);
            }

            int id = getResources().getIdentifier(source, "drawable", context.getPackageName());

            if (id == 0) {
                // the drawable resource wasn't found in our package, maybe it is a stock android drawable?
                id = getResources().getIdentifier(source, "drawable", "android");
            }

            if (id == 0) {
                // prevent a crash if the resource still can't be found
                return null;
            }
            else {
                final Drawable d = DrawableUtils.getTintedDrawable(AppInfoFragment.this.getContext(), id, textColor);
                if (d != null) {
                    d.setBounds(0,0,d.getIntrinsicWidth(),d.getIntrinsicHeight());
                }
                return d;
            }
        }

    }

}
