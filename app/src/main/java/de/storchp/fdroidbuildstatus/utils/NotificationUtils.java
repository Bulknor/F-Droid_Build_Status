package de.storchp.fdroidbuildstatus.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import de.storchp.fdroidbuildstatus.MainActivity;
import de.storchp.fdroidbuildstatus.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationUtils {

    private static final String CHANNEL_ID = "general";
    private static final String TAG = NotificationUtils.class.getSimpleName();

    public static void createNewBuildNotification(final Context context, final String text) {
        Log.d(TAG, "new notification: " + text);

        final Intent intent = new Intent(context, MainActivity.class);
        final PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(text)
                .setSmallIcon(R.drawable.ic_fdroid_buildstatus)
                .setContentIntent(pIntent)
                .setAutoCancel(true);

        final Notification notification = builder.build();
        final NotificationManager notificationManager = getNotificationManager(context);
        notificationManager.notify(0, notification);
    }

    public static void cancelNotification(final Context context) {
        final NotificationManager notificationManager = getNotificationManager(context);
        notificationManager.cancel(0);
    }

    private static NotificationManager getNotificationManager(final Context context) {
        return (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
    }

    public static void createNotificationChannel(final Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final NotificationManager notificationManager = getNotificationManager(context);
            final NotificationChannel channel = new NotificationChannel(CHANNEL_ID, context.getString(R.string.channel_name), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(context.getString(R.string.channel_description));
            channel.enableVibration(true);
            notificationManager.createNotificationChannel(channel);
        }
    }

}
