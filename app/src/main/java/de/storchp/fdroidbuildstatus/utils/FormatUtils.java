package de.storchp.fdroidbuildstatus.utils;

import android.net.Uri;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormatUtils {

    public static String formatShortDateTime(final long millis) {
        return new SimpleDateFormat("dd/MM hh:mm", Locale.getDefault()).format(new Date(millis));
    }

    public static String formatVersion(final String versionCode, final String version) {
        return (TextUtils.isEmpty(versionCode) ? "" : versionCode)
                + (TextUtils.isEmpty(version) ? "" : " - " + version);
    }

    public static String getNameFromSource(final String sourceCode) {
        try {
            return Uri.parse(sourceCode).getLastPathSegment();
        } catch (final Exception ignored) {
        }
        return null;
    }

}
