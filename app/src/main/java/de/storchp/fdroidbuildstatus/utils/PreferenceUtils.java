package de.storchp.fdroidbuildstatus.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import androidx.preference.PreferenceManager;

import java.util.Set;

import de.storchp.fdroidbuildstatus.Constants;
import de.storchp.fdroidbuildstatus.R;
import de.storchp.fdroidbuildstatus.model.BuildLoadType;
import de.storchp.fdroidbuildstatus.model.BuildRunType;

public class PreferenceUtils {

    private static final String TAG = PreferenceUtils.class.getSimpleName();

    private static SharedPreferences getSharedPreferences(final Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isUpdateCheckEnabled(final Context context) {
        return getSharedPreferences(context).getBoolean(context.getString(R.string.PREF_UPDATE_CHECK), true);
    }

    public static boolean isNotifyFavouritesOnly(final Context context) {
        return getSharedPreferences(context).getBoolean(context.getString(R.string.PREF_NOTIFY_FAVOURITES_ONLY), false);
    }

    public static boolean isShowFavouritesWithoutBuildStatus(final Context context) {
        return getSharedPreferences(context).getBoolean(context.getString(R.string.PREF_SHOW_UNKNWON_FAVOURITE_BUILDS), true);
    }

    public static BuildLoadType getBuildLoadType(final Context context) {
        try {
            return BuildLoadType.valueOf(getSharedPreferences(context).getString(context.getString(R.string.PREF_LOAD_BUILD_RUN_TYPE), BuildLoadType.MERGED.toString()));
        } catch (final Exception ignored) {
            return BuildLoadType.FINISHED;
        }
    }

    public static void setBuildLoadType(final Context context, final BuildLoadType buildLoadType) {
        putString(context, R.string.PREF_LOAD_BUILD_RUN_TYPE, buildLoadType.name());
    }

    public static long getUpdateInterval(final Context context) {
        try {
            return Long.parseLong(getSharedPreferences(context).getString(context.getString(R.string.PREF_UPDATE_INTERVAL), String.valueOf(Constants.TIME.TWO_HOURS)));
        } catch (final NumberFormatException ignored) {
            return Constants.TIME.TWO_HOURS;
        }
    }

    public static BuildRunType getBuildCheckType(final Context context) {
        try {
            return BuildRunType.valueOf(getSharedPreferences(context).getString(context.getString(R.string.PREF_BUILD_RUN_TYPE), BuildRunType.FINISHED.toString()));
        } catch (final Exception ignored) {
            return BuildRunType.FINISHED;
        }
    }

    private static void putBoolean(final Context context, final int key, final boolean value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(context.getString(key), value);
        editor.apply();
    }

    private static void putString(final Context context, final int key, final String value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        if (value == null) {
            editor.remove(context.getString(key));
        } else {
            editor.putString(context.getString(key), value.trim());
        }
        editor.apply();
    }

    private static void putStringSet(final Context context, final int key, final Set<String> value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putStringSet(context.getString(key), value);
        editor.apply();
    }

    private static void putLong(final Context context, final int key, final long value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putLong(context.getString(key), value);
        editor.apply();
    }

    private static void putDouble(final Context context, final int key, final double value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putLong(context.getString(key), Double.doubleToRawLongBits(value));
        editor.apply();
    }

    private static double getDouble(final Context context, final int key, final double defaultValue) {
        if ( !getSharedPreferences(context).contains(context.getString(key)))
            return defaultValue;

        return Double.longBitsToDouble(getSharedPreferences(context).getLong(context.getString(key), 0));
    }

    private static Boolean getOptionalBoolean(final Context context, final int key) {
        if (getSharedPreferences(context).contains(context.getString(key))) {
            final String stringValue = getSharedPreferences(context).getString(context.getString(key), "false");
            return Boolean.valueOf(stringValue);
        }
        return null;
    }

    private static void putUri(final Context context, final int key, final Uri uri) {
        putString(context, key, uri != null ? uri.toString() : null);
    }

    private static Uri getUri(final Context context, final String key) {
        return toUri(getSharedPreferences(context).getString(key, null));
    }

    public static Uri toUri(final String uriString) {
        try {
            return Uri.parse(uriString);
        } catch (final Exception ignored) {
            Log.e(TAG, "can't read Uri string " + uriString);
        }
        return null;
    }

    public static long getLastUpdateLoaded(final Context context) {
        return getSharedPreferences(context).getLong(context.getString(R.string.PREF_LAST_UPDATE_LOADED), 0L);
    }

    public static void setLastUpdateLoaded(final Context context, final long lastUpdateLoaded) {
        putLong(context, R.string.PREF_LAST_UPDATE_LOADED, lastUpdateLoaded);
    }

    public static void setRepoIndexLoaded(final Context context) {
        putBoolean(context, R.string.PREF_REPO_INDEX_LOADED, true);
    }

    public static boolean isRepoIndexLoaded(final Context context) {
        return getSharedPreferences(context).getBoolean(context.getString(R.string.PREF_REPO_INDEX_LOADED), false);
    }

}
