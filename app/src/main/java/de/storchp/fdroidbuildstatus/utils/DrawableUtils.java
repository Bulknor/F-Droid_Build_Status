package de.storchp.fdroidbuildstatus.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

public class DrawableUtils {

    public static void setMenuIconTint(final Context context, final Menu menu, final int itemId, final int color) {
        Drawable drawable = menu.findItem(itemId).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(context, color));
        menu.findItem(itemId).setIcon(drawable);
    }

    public static void setIconWithTint(final Context context, final ImageView imageView, final int imageId, final int color) {
        imageView.setImageDrawable(getTintedDrawable(context, imageId, color));
    }

    public static void setCompoundDrawablesLeft(final Context context, final TextView textView, final int imageId, final int color) {
        textView.setCompoundDrawablesWithIntrinsicBounds(getTintedDrawable(context, imageId, color), null, null, null);
    }

    public static void setCompoundDrawablesRight(final Context context, final TextView textView, final int imageId, final int color) {
        textView.setCompoundDrawablesWithIntrinsicBounds(null, null, getTintedDrawable(context, imageId, color), null);
    }

    public static Drawable getTintedDrawable(final Context context, final int imageId, final int color) {
        if (imageId > 0) {
            final Drawable unwrappedDrawable = ContextCompat.getDrawable(context, imageId);
            final Drawable wrappedDrawable;
            if (unwrappedDrawable != null) {
                wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                DrawableCompat.setTint(wrappedDrawable, color);
                return wrappedDrawable;
            }
        }
        return null;
    }

}
