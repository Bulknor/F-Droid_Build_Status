package de.storchp.fdroidbuildstatus.api;

import java.util.HashSet;
import java.util.Set;

public class Update {

    private Set<String> disabled = new HashSet<>();

    private Set<String> needsUpdate = new HashSet<>();

    public Set<String> getNeedsUpdate() {
        return needsUpdate;
    }

    public Set<String> getDisabled() {
        return disabled;
    }
}
