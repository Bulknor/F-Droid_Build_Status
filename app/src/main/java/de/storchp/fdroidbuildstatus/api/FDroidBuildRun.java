package de.storchp.fdroidbuildstatus.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.storchp.fdroidbuildstatus.model.BuildRun;
import de.storchp.fdroidbuildstatus.model.BuildRunType;

public class FDroidBuildRun extends BuildRun {

    private final Set<SuccessBuildItem> successfulBuilds = new HashSet<>();

    private final Set<FailedBuildItem> failedBuilds = new HashSet<>();

    private final Set<SuccessBuildIdItem> successfulBuildIds = new HashSet<>();

    private final FdroidData fdroiddata = new FdroidData();

    public Set<SuccessBuildItem> getSuccessfulBuilds() {
        return successfulBuilds;
    }

    public Set<FailedBuildItem> getFailedBuilds() {
        return failedBuilds;
    }

    public List<BuildItem> getAllBuilds() {
        final List<BuildItem> allBuilds = new ArrayList<>(failedBuilds);
        allBuilds.addAll(successfulBuildIds);
        return allBuilds;
    }

    public void setBuildRunType(final BuildRunType buildRunType) {
        super.setBuildRunType(buildRunType);
        for (final BuildItem item : failedBuilds) {
            item.setBuildRunType(buildRunType);
        }
        for (final BuildItem item : successfulBuildIds) {
            item.setBuildRunType(buildRunType);
        }
    }

    public void merge(final FDroidBuildRun buildRun) {
        successfulBuilds.addAll(buildRun.successfulBuilds);
        failedBuilds.addAll(buildRun.failedBuilds);
    }

    public FdroidData getFdroiddata() {
        return fdroiddata;
    }

    public Set<SuccessBuildIdItem> getSuccessfulBuildIds() {
        return successfulBuildIds;
    }

    public int getSuccessCount() {
        return successfulBuildIds.size();
    }

    public void setSuccessCount(final int successCount) {
        // ignored
    }

    public int getFailedCount() {
        return failedBuilds.size();
    }

    public void setFailedCount(final int failedCount) {
        // ignored
    }

    public String getCommitId() {
        return fdroiddata.getCommitId();
    }

    public void setCommitId(final String commitId) {
        this.fdroiddata.setCommitId(commitId);
    }

    public String findVersionFor(final String id, final String versionCode) {
        for (final BuildItem item : successfulBuilds) {
            if (item.getId().equals(id)) {
                for (final Build build: item.getBuilds()) {
                    if (build.getVersionCode().equals(versionCode)) {
                        return build.getVersionName();
                    }
                }
            }
        }
        return null;
    }

}
