package de.storchp.fdroidbuildstatus.api;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.annotations.JsonAdapter;

import java.lang.reflect.Type;

import de.storchp.fdroidbuildstatus.R;
import de.storchp.fdroidbuildstatus.model.BuildStatus;

@JsonAdapter(SuccessBuildIdItem.Deserializer.class)
public class SuccessBuildIdItem extends BuildItem {

    public SuccessBuildIdItem() {
        super();
        setStatus(BuildStatus.SUCCESS);
    }

    public String getFullVersion(final Context context) {
        return context.getString(R.string.success_build_version) + getCurrentVersionCode();
    }

    public static class Deserializer implements JsonDeserializer<SuccessBuildIdItem> {

        @Override
        public SuccessBuildIdItem deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) {
            try {
                final SuccessBuildIdItem item = new SuccessBuildIdItem();
                if (json.isJsonArray()) {
                    final JsonArray elements = json.getAsJsonArray();
                    item.setId(elements.get(0).getAsString());
                    item.setCurrentVersionCode(elements.get(1).getAsString());
                }
                return item;
            } catch (final Exception e) {
                return null;
            }
        }
    }

}
