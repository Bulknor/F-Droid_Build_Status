package de.storchp.fdroidbuildstatus.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.annotations.JsonAdapter;

import java.lang.reflect.Type;

import de.storchp.fdroidbuildstatus.model.BuildStatus;

@JsonAdapter(FailedBuildItem.Deserializer.class)
public class FailedBuildItem extends BuildItem {

    public FailedBuildItem() {
        super();
        setStatus(BuildStatus.FAILED);
    }

    public static class Deserializer implements JsonDeserializer<FailedBuildItem> {

        @Override
        public FailedBuildItem deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) {
            try {
                final FailedBuildItem item = new FailedBuildItem();
                if (json.isJsonArray()) {
                    final JsonArray elements = json.getAsJsonArray();
                    item.setId(elements.get(0).getAsString());
                    item.setCurrentVersionCode(elements.get(1).getAsString());
                    item.setLog(elements.get(2).getAsString());
                }
                return item;
            } catch (final Exception e) {
                return null;
            }
        }
    }

}
