package de.storchp.fdroidbuildstatus.api;

public class FdroidData {

    private String commitId;

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(final String commitId) {
        this.commitId = commitId;
    }

}
