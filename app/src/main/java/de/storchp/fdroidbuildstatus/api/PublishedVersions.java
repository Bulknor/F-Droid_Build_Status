package de.storchp.fdroidbuildstatus.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PublishedVersions {

    private String packageName;
    private String suggestedVersionCode;
    private final List<PublishedPackage> packages = new ArrayList<>();

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(final String packageName) {
        this.packageName = packageName;
    }

    public String getSuggestedVersionCode() {
        return suggestedVersionCode;
    }

    public void setSuggestedVersionCode(final String suggestedVersionCode) {
        this.suggestedVersionCode = suggestedVersionCode;
    }

    public List<PublishedPackage> getPackages() {
        return packages;
    }

    public static class PublishedPackage {
        private String versionName;

        private String versionCode;

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(final String versionName) {
            this.versionName = versionName;
        }

        public String getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(final String versionCode) {
            this.versionCode = versionCode;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (!(o instanceof PublishedPackage)) return false;
            final PublishedPackage that = (PublishedPackage) o;
            return versionName.equals(that.versionName) &&
                    versionCode.equals(that.versionCode);
        }

        @Override
        public int hashCode() {
            return Objects.hash(versionName, versionCode);
        }

    }
}
