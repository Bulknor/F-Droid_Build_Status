package de.storchp.fdroidbuildstatus.api;

import android.os.Build;

import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Date;

import de.storchp.fdroidbuildstatus.BuildConfig;
import de.storchp.fdroidbuildstatus.utils.UserAgentInterceptor;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface FDroidAPI {

    @Headers("Cache-Control: no-cache")
    @GET("/repo/status/build.json")
    Call<FDroidBuildRun> getFinishedBuildRun();

    @Headers("Cache-Control: no-cache")
    @GET("/repo/status/running.json")
    Call<FDroidBuildRun> getRunningBuildRun();

    @GET("/repo/status/update.json")
    Call<Update> getUpdate();

    @GET("/repo/{id}_{versionCode}.log.gz")
    Call<ResponseBody> getBuildLog(@Path("id") final String id, @Path("versionCode") final String versionCode);

    @GET("/api/v1/packages/{id}")
    Call<PublishedVersions> getPublishedVersions(@Path("id") final String id);

    @GET("/repo/index-v1.jar")
    Call<ResponseBody> getIndex();

    static Date getLastModified(final retrofit2.Response<FDroidBuildRun> response) {
        Date date = response.headers().getDate("Last-Modified");
        if (date == null) {
            date = new Date(); // fallback
        }
        return date;
    }

    static FDroidAPI create() {
        final GsonBuilder gson = new GsonBuilder();
        gson.registerTypeAdapter(FailedBuildItem.class, new FailedBuildItem.Deserializer());

        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

        final OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new UserAgentInterceptor());

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor);
        }

        final OkHttpClient okHttp = builder.build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://f-droid.org")
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create(gson.create()))
                .build();

        return retrofit.create(FDroidAPI.class);
    }

}
