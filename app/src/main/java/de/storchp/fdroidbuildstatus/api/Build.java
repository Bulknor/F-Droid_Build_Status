package de.storchp.fdroidbuildstatus.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Build {

    private String versionCode;
    private String versionName;

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(final String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(final String versionName) {
        this.versionName = versionName;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Build build = (Build) o;
        return Objects.equals(versionCode, build.versionCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(versionCode);
    }
}
