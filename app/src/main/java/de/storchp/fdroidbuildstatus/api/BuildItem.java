package de.storchp.fdroidbuildstatus.api;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import de.storchp.fdroidbuildstatus.model.BuildRunType;
import de.storchp.fdroidbuildstatus.model.BuildStatus;

import static de.storchp.fdroidbuildstatus.utils.FormatUtils.getNameFromSource;

public class BuildItem {

    protected String log;
    private String id;
    @SerializedName("AutoName") private String autoName;
    @SerializedName("Name") private String name;
    @SerializedName("CurrentVersion") private String currentVersion;
    @SerializedName("CurrentVersionCode") private String currentVersionCode;
    private BuildStatus status = BuildStatus.UNKNOWN;
    private BuildRunType buildRunType = BuildRunType.NONE;
    private String metadatapath;
    @SerializedName("SourceCode") private String sourceCode;
    @SerializedName("Builds") private final Set<Build> builds = new HashSet<>();

    public BuildItem() {
        super();
    }

    public String getAppName() {
        if (!TextUtils.isEmpty(name)) {
            return name;
        }
        if (!TextUtils.isEmpty(autoName) && !autoName.startsWith("${")) {
            return autoName;
        }
        final String nameFromSource = getNameFromSource(getSourceCode());
        if (!TextUtils.isEmpty(nameFromSource)) {
            return nameFromSource;
        }
        return "";
    }

    public boolean hasProperName() {
        return !TextUtils.isEmpty(name) || (!TextUtils.isEmpty(autoName) && !autoName.startsWith("${"));
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setCurrentVersion(final String currentVersion) {
        this.currentVersion = currentVersion;
    }

    public String getCurrentVersion() {
        return this.currentVersion;
    }

    public void setCurrentVersionCode(final String currentVersionCode) {
        this.currentVersionCode = currentVersionCode;
    }

    public String getCurrentVersionCode() {
        return currentVersionCode;
    }

    public BuildStatus getStatus() {
        return status;
    }

    public void setStatus(final BuildStatus status) {
        this.status = status;
    }

    public void setLog(final String log) {
        this.log = log;
    }

    public String getLog() {
        return log;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public BuildRunType getBuildRunType() {
        return buildRunType;
    }

    public void setBuildRunType(final BuildRunType buildRunType) {
        this.buildRunType = buildRunType;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof BuildItem)) return false;
        final BuildItem buildItem = (BuildItem) o;
        return Objects.equals(id, buildItem.id) &&
                Objects.equals(currentVersionCode, buildItem.currentVersionCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, currentVersionCode);
    }

    public String getMetadatapath() {
        return metadatapath;
    }

    public void setMetadatapath(final String metadatapath) {
        this.metadatapath = metadatapath;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(final String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public Set<Build> getBuilds() {
        return builds;
    }

}
