package de.storchp.fdroidbuildstatus.api;

import de.storchp.fdroidbuildstatus.model.BuildStatus;

public class SuccessBuildItem extends BuildItem {

    public SuccessBuildItem() {
        super();
        setStatus(BuildStatus.SUCCESS);
    }

}
