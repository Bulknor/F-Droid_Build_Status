package de.storchp.fdroidbuildstatus.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Metadata {

    @JsonProperty("Builds")
    private Set<Build> builds = new HashSet<>();

    public Set<Build> getBuilds() {
        return builds;
    }

    public Build getHighestVersion() {
        Build highestVersion = null;
        for (final Build build : builds) {
            if (highestVersion == null || Integer.parseInt(build.getVersionCode()) > Integer.parseInt(highestVersion.getVersionCode())) {
                highestVersion = build;
            }
        }

        return highestVersion;
    }
}
