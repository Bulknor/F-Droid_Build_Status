package de.storchp.fdroidbuildstatus.api;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.storchp.fdroidbuildstatus.utils.FormatUtils.getNameFromSource;

public class Index {

    private final List<App> apps = new ArrayList<>();

    public List<App> getApps() {
        return apps;
    }

    public static class App {
        private String name;
        private String packageName;
        private String sourceCode;
        private final Map<String, LocalizedName> localized = new HashMap<>();

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public String getPackageName() {
            return packageName;
        }

        public String getSourceCode() {
            return sourceCode;
        }

        public Map<String, LocalizedName> getLocalized() {
            return localized;
        }

        public String getAppName() {
            if (!TextUtils.isEmpty(name)) {
                return name;
            }
            String localizedName = null;
            for (final Map.Entry<String, LocalizedName> entry : localized.entrySet()) {
                if (!TextUtils.isEmpty(entry.getValue().getName())) {
                    localizedName = entry.getValue().getName();
                    if (entry.getKey().startsWith("en")) {
                        return entry.getValue().getName();
                    }
                }
            }
            if (!TextUtils.isEmpty(localizedName)) {
                return localizedName;
            }

            final String nameFromSource = getNameFromSource(getSourceCode());
            if (!TextUtils.isEmpty(nameFromSource)) {
                return nameFromSource;
            }

            return "";
        }
    }

    public static class LocalizedName {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }
    }
}
