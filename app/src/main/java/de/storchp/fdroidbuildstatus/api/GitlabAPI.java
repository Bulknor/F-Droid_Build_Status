package de.storchp.fdroidbuildstatus.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import de.storchp.fdroidbuildstatus.BuildConfig;
import de.storchp.fdroidbuildstatus.utils.UserAgentInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitlabAPI {

    @GET("/fdroid/fdroiddata/-/raw/master/metadata/{id}.yml")
    Call<Metadata> getFdroidDataMetadata(@Path("id") final String id);

    static GitlabAPI create() {
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

        final OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new UserAgentInterceptor());

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor);
        }

        final OkHttpClient okHttp = builder.build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://gitlab.com")
                .client(okHttp)
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .build();

        return retrofit.create(GitlabAPI.class);
    }

}
