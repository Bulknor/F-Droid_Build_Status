package de.storchp.fdroidbuildstatus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.storchp.fdroidbuildstatus.api.BuildItem;
import de.storchp.fdroidbuildstatus.api.FDroidBuildRun;
import de.storchp.fdroidbuildstatus.api.Index;
import de.storchp.fdroidbuildstatus.api.SuccessBuildItem;
import de.storchp.fdroidbuildstatus.api.Update;
import de.storchp.fdroidbuildstatus.model.App;
import de.storchp.fdroidbuildstatus.model.AppBuild;
import de.storchp.fdroidbuildstatus.model.AppNotification;
import de.storchp.fdroidbuildstatus.model.BuildRun;
import de.storchp.fdroidbuildstatus.model.BuildRunType;
import de.storchp.fdroidbuildstatus.model.BuildStatus;

public class DbAdapter {

    private static final String TAG = de.storchp.fdroidbuildstatus.DbAdapter.class.getSimpleName();

    private static final String DATABASE_NAME = "fdroidbuildstatus.db";

    private static final String DATABASE_TABLE_APPS = "apps";
    private static final String DATABASE_TABLE_BUILD_RUNS = "build_runs";
    private static final String DATABASE_TABLE_BUILDS = "builds";
    private static final String DATABASE_TABLE_DISABLED = "disabled";
    private static final String DATABASE_TABLE_NEEDS_UPDATE = "needs_update";
    private static final String DATABASE_TABLE_NOTIFICATIONS = "notifications";

    private static final int DATABASE_VERSION = 12;

    private static final String CREATE_STATEMENT_APPS = "CREATE TABLE " + DATABASE_TABLE_APPS + " ("
            + Constants.APPS.ID + " TEXT PRIMARY KEY,"
            + Constants.APPS.NAME + " TEXT,"
            + Constants.APPS.FAVOURITE + " INTEGER,"
            + Constants.APPS.DISABLED + " INTEGER,"
            + Constants.APPS.NEEDS_UPDATE + " INTEGER,"
            + Constants.APPS.SOURCE_CODE + " TEXT)";

    private static final String CREATE_STATEMENT_BUILD_RUNS = "CREATE TABLE " + DATABASE_TABLE_BUILD_RUNS + " ("
            + Constants.BUILD_RUNS.BUILD_RUN_TYPE + " TEXT PRIMARY KEY,"
            + Constants.BUILD_RUNS.START + " INTEGER,"
            + "\"" + Constants.BUILD_RUNS.END + "\"" + " INTEGER,"
            + Constants.BUILD_RUNS.LAST_MODIFIED + " INTEGER,"
            + Constants.BUILD_RUNS.LAST_UPDATED + " INTEGER,"
            + Constants.BUILD_RUNS.MAX_BUILD_TIME_REACHED + " INTEGER,"
            + Constants.BUILD_RUNS.SUBCOMMAND + " TEXT,"
            + Constants.BUILD_RUNS.DATA_COMMIT_ID + " TEXT)";

    private static final String CREATE_STATEMENT_BUILDS = "CREATE TABLE " + DATABASE_TABLE_BUILDS + " ("
            + Constants.BUILDS.BUILD_RUN_TYPE + " TEXT,"
            + Constants.BUILDS.ID + " TEXT,"
            + Constants.BUILDS.VERSION_CODE + " TEXT,"
            + Constants.BUILDS.VERSION + " TEXT,"
            + Constants.BUILDS.STATUS + " TEXT,"
            + Constants.BUILDS.LOG + " TEXT,"
            + Constants.BUILDS.METADATA_PATH + " TEXT,"
            + "PRIMARY KEY (" + Constants.BUILDS.BUILD_RUN_TYPE + ", " + Constants.BUILDS.ID + ", " + Constants.BUILDS.VERSION_CODE + "))";

    private static final String CREATE_STATEMENT_DISABLED = "CREATE TABLE " + DATABASE_TABLE_DISABLED + " ("
            + Constants.APPS.ID + " TEXT PRIMARY KEY)";

    private static final String CREATE_STATEMENT_NEEDS_UPDATE = "CREATE TABLE " + DATABASE_TABLE_NEEDS_UPDATE + " ("
            + Constants.APPS.ID + " TEXT PRIMARY KEY)";

    private static final String CREATE_STATEMENT_NOTIFICATIONS = "CREATE TABLE " + DATABASE_TABLE_NOTIFICATIONS + " ("
            + Constants.NOTIFICATIONS.BUILD_RUN_TYPE + " TEXT,"
            + Constants.NOTIFICATIONS.START + " INTEGER,"
            + Constants.NOTIFICATIONS.ID + " TEXT,"
            + Constants.NOTIFICATIONS.VERSION_CODE + " TEXT,"
            + "PRIMARY KEY (" + Constants.NOTIFICATIONS.BUILD_RUN_TYPE + ", " + Constants.NOTIFICATIONS.START + ", " + Constants.NOTIFICATIONS.ID + ", " + Constants.NOTIFICATIONS.VERSION_CODE + "))";

    private final Context context;
    private DbOpenHelper dbHelper;
    private SQLiteDatabase db;

    public DbAdapter(final Context context) {
        this.context = context;
    }

    public void open() {
        dbHelper = new DbOpenHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        db.close();
        dbHelper.close();
    }

    public void toggleFavourite(final String id) {
        try (final Cursor cursor = db.query(DATABASE_TABLE_APPS, null, Constants.APPS.ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null)) {
            final ContentValues values = new ContentValues();
            if (cursor.moveToFirst()) {
                final int fav = cursor.getInt(cursor.getColumnIndex(Constants.APPS.FAVOURITE));
                values.put(Constants.APPS.FAVOURITE, fav == 0 ? 1 : 0);
                db.update(DATABASE_TABLE_APPS, values, Constants.APPS.ID + " = ?", new String[]{id});
                return;
            }
            values.put(Constants.APPS.ID, id);
            values.put(Constants.APPS.FAVOURITE, 1);
            db.insert(DATABASE_TABLE_APPS, null, values);
        }
    }

    @NonNull
    public Map<String, de.storchp.fdroidbuildstatus.model.App> getFavourites() {
        final Map<String, de.storchp.fdroidbuildstatus.model.App> apps = new HashMap<>();
        try (final Cursor cursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_APPS + " WHERE " + Constants.APPS.FAVOURITE + " = 1", null)) {
            while (cursor.moveToNext()) {
                final de.storchp.fdroidbuildstatus.model.App app = createAppFromCursor(cursor);
                apps.put(app.getId(), app);
            }
        }
        return apps;
    }

    @NonNull
    private de.storchp.fdroidbuildstatus.model.App createAppFromCursor(@NonNull final Cursor cursor) {
        final de.storchp.fdroidbuildstatus.model.App app = new de.storchp.fdroidbuildstatus.model.App();
        app.setId(cursor.getString(cursor.getColumnIndex(Constants.APPS.ID)));
        app.setName(cursor.getString(cursor.getColumnIndex(Constants.APPS.NAME)));
        app.setFavourite(cursor.getInt(cursor.getColumnIndex(Constants.APPS.FAVOURITE)) == 1);
        app.setDisabled(cursor.getInt(cursor.getColumnIndex(Constants.APPS.DISABLED)) == 1);
        app.setNeedsUpdate(cursor.getInt(cursor.getColumnIndex(Constants.APPS.NEEDS_UPDATE)) == 1);
        app.setSourceCode(cursor.getString(cursor.getColumnIndex(Constants.APPS.SOURCE_CODE)));
        return app;
    }

    public void updateApps(final Set<SuccessBuildItem> successfulBuilds) {
        for (final SuccessBuildItem item : successfulBuilds) {
            try (final Cursor cursor = db.query(DATABASE_TABLE_APPS, null, Constants.APPS.ID + "=?",
                    new String[]{String.valueOf(item.getId())}, null, null, null)) {
                final ContentValues values = new ContentValues();
                if (cursor.moveToFirst()) {
                    if (item.hasProperName()) {
                        values.put(Constants.APPS.NAME, item.getAppName());
                        if (item.getSourceCode() != null) {
                            values.put(Constants.APPS.SOURCE_CODE, item.getSourceCode());
                        }
                        db.update(DATABASE_TABLE_APPS, values, Constants.APPS.ID + " = ?", new String[]{item.getId()});
                    }
                } else {
                    values.put(Constants.APPS.ID, item.getId());
                    values.put(Constants.APPS.NAME, item.getAppName());
                    values.put(Constants.APPS.SOURCE_CODE, item.getSourceCode());
                    db.insert(DATABASE_TABLE_APPS, null, values);
                }
            }
        }
    }

    public void updateApps(final Collection<Index.App> apps) {
        for (final Index.App app : apps) {
            try (final Cursor cursor = db.query(DATABASE_TABLE_APPS, null, Constants.APPS.ID + "=?",
                    new String[]{String.valueOf(app.getPackageName())}, null, null, null)) {
                final ContentValues values = new ContentValues();
                if (cursor.moveToFirst()) {
                    values.put(Constants.APPS.NAME, app.getAppName());
                    if (app.getSourceCode() != null) {
                        values.put(Constants.APPS.SOURCE_CODE, app.getSourceCode());
                    }
                    db.update(DATABASE_TABLE_APPS, values, Constants.APPS.ID + " = ?", new String[]{app.getPackageName()});
                } else {
                    values.put(Constants.APPS.ID, app.getPackageName());
                    values.put(Constants.APPS.NAME, app.getAppName());
                    values.put(Constants.APPS.SOURCE_CODE, app.getSourceCode());
                    db.insert(DATABASE_TABLE_APPS, null, values);
                }
            }
        }

    }

    public void saveBuildRun(final FDroidBuildRun buildRun) {
        Log.i(TAG, "Save new buildRun " + buildRun.getBuildRunType() + " " + buildRun.getLastModified());
        db.beginTransaction();

        db.delete(DATABASE_TABLE_BUILD_RUNS, Constants.BUILD_RUNS.BUILD_RUN_TYPE + "=?", new String[]{buildRun.getBuildRunType().name()});
        final ContentValues valuesBuildRun = new ContentValues();
        valuesBuildRun.put(Constants.BUILD_RUNS.BUILD_RUN_TYPE, buildRun.getBuildRunType().name());
        valuesBuildRun.put(Constants.BUILD_RUNS.START, buildRun.getStartTimestamp());
        valuesBuildRun.put(Constants.BUILD_RUNS.END, buildRun.getEndTimestamp());
        valuesBuildRun.put(Constants.BUILD_RUNS.LAST_MODIFIED, buildRun.getLastModified().getTime());
        valuesBuildRun.put(Constants.BUILD_RUNS.LAST_UPDATED, System.currentTimeMillis());
        valuesBuildRun.put(Constants.BUILD_RUNS.MAX_BUILD_TIME_REACHED, buildRun.isMaxBuildTimeReached() ? 1 : 0);
        valuesBuildRun.put(Constants.BUILD_RUNS.SUBCOMMAND, buildRun.getSubcommand());
        valuesBuildRun.put(Constants.BUILD_RUNS.DATA_COMMIT_ID, buildRun.getFdroiddata().getCommitId());
        db.insert(DATABASE_TABLE_BUILD_RUNS, null, valuesBuildRun);

        db.delete(DATABASE_TABLE_BUILDS, Constants.BUILDS.BUILD_RUN_TYPE + "=?", new String[]{buildRun.getBuildRunType().name()});
        final Set<BuildItem> mergedBuildItems = new HashSet<>(buildRun.getFailedBuilds());
        mergedBuildItems.addAll(buildRun.getSuccessfulBuildIds()); // add successful builds if not already contained with same versionCode
        for (BuildItem item : mergedBuildItems) {
            if (item.getId() == null) {
                continue;
            }
            final ContentValues valuesBuild = new ContentValues();
            valuesBuild.put(Constants.BUILDS.BUILD_RUN_TYPE, item.getBuildRunType().name());
            valuesBuild.put(Constants.BUILDS.ID, item.getId());
            valuesBuild.put(Constants.BUILDS.VERSION, item.getCurrentVersion());
            if (item.getCurrentVersion() == null) {
                valuesBuild.put(Constants.BUILDS.VERSION, buildRun.findVersionFor(item.getId(), item.getCurrentVersionCode()));
            }
            valuesBuild.put(Constants.BUILDS.STATUS, item.getStatus().name());
            valuesBuild.put(Constants.BUILDS.LOG, item.getLog());


            if (item.getStatus() == BuildStatus.SUCCESS) {
                for (final BuildItem successItem : buildRun.getSuccessfulBuilds()) {
                    if (successItem.equals(item)) {
                        item = successItem; // overwrite item with details from SuccessfulBuilds
                    }
                }
            }
            valuesBuild.put(Constants.BUILDS.VERSION_CODE, item.getCurrentVersionCode());
            valuesBuild.put(Constants.BUILDS.METADATA_PATH, item.getMetadatapath());
            db.insert(DATABASE_TABLE_BUILDS, null, valuesBuild);
        }

        updateApps(buildRun.getSuccessfulBuilds());

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    @NonNull
    public Map<BuildRunType, BuildRun> loadBuildRuns(final Set<BuildRunType> buildRunTypes) {
        final Map<BuildRunType, BuildRun> buildRuns = new HashMap<>();

        try (final Cursor cursorBuildRuns = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_BUILD_RUNS + " WHERE " + Constants.BUILD_RUNS.BUILD_RUN_TYPE + " IN (" + makePlaceholders(buildRunTypes.size()) + ")", toArray(buildRunTypes))) {
            while (cursorBuildRuns.moveToNext()) {
                final BuildRun buildRun = createBuildRunFromCursor(cursorBuildRuns);
                buildRuns.put(buildRun.getBuildRunType(), buildRun);
                try (final Cursor cursorBuildCount = db.rawQuery("SELECT " + Constants.BUILDS.STATUS + ", count(*) AS " + Constants.COUNTER + " FROM " + DATABASE_TABLE_BUILDS + " WHERE " + Constants.BUILDS.BUILD_RUN_TYPE + " = ? GROUP BY " + Constants.BUILDS.STATUS, new String[]{buildRun.getBuildRunType().name()})) {
                    while (cursorBuildCount.moveToNext()) {
                        final int count = cursorBuildCount.getInt(cursorBuildCount.getColumnIndex(Constants.COUNTER));
                        switch (BuildStatus.valueOf(cursorBuildCount.getString(cursorBuildCount.getColumnIndex(Constants.BUILDS.STATUS)))) {
                            case SUCCESS:
                                buildRun.setSuccessCount(count);
                                break;
                            case FAILED:
                                buildRun.setFailedCount(count);
                                break;
                        }
                    }
                }
            }
        }
        return buildRuns;
    }

    public List<de.storchp.fdroidbuildstatus.model.App> loadAppBuilds(final Set<BuildRunType> buildRunTypes, final boolean includeFavoritesWithoutBuilds) {
        final Map<String, de.storchp.fdroidbuildstatus.model.App> favourites = includeFavoritesWithoutBuilds ? getFavourites() : Collections.emptyMap();
        final List<de.storchp.fdroidbuildstatus.model.App> apps = new ArrayList<>();
        de.storchp.fdroidbuildstatus.model.App app = null;
        try (final Cursor cursor = db.rawQuery("SELECT b." + Constants.BUILDS.BUILD_RUN_TYPE
                + ", b." + Constants.BUILDS.ID
                + ", b." + Constants.BUILDS.STATUS
                + ", b." + Constants.BUILDS.VERSION
                + ", b." + Constants.BUILDS.VERSION_CODE
                + ", b." + Constants.BUILDS.METADATA_PATH
                + ", a." + Constants.APPS.SOURCE_CODE
                + ", a." + Constants.APPS.NAME
                + ", a." + Constants.APPS.FAVOURITE
                + ", r." + Constants.BUILD_RUNS.DATA_COMMIT_ID
                + ", a." + Constants.APPS.DISABLED
                + ", a." + Constants.APPS.NEEDS_UPDATE
                + " FROM " + DATABASE_TABLE_BUILDS + " b "
                + " LEFT JOIN " + DATABASE_TABLE_APPS + " a ON b." + Constants.BUILDS.ID + " = a." + Constants.APPS.ID
                + " LEFT JOIN " + DATABASE_TABLE_BUILD_RUNS + " r ON r." + Constants.BUILD_RUNS.BUILD_RUN_TYPE + " = b." + Constants.BUILD_RUNS.BUILD_RUN_TYPE
                + " WHERE b." + Constants.BUILDS.BUILD_RUN_TYPE + " IN (" + makePlaceholders(buildRunTypes.size()) + ")"
                + " ORDER BY b." + Constants.BUILDS.ID, toArray(buildRunTypes))) {
            while (cursor.moveToNext()) {
                final String id = cursor.getString(cursor.getColumnIndex(Constants.BUILDS.ID));
                if (app == null || !app.getId().equals(id)) {
                    app = new de.storchp.fdroidbuildstatus.model.App(id, cursor.getString(cursor.getColumnIndex(Constants.APPS.NAME)));
                    app.setFavourite(cursor.getInt(cursor.getColumnIndex(Constants.APPS.FAVOURITE)) == 1);
                    app.setDisabled(cursor.getInt(cursor.getColumnIndex(Constants.APPS.DISABLED)) == 1);
                    app.setNeedsUpdate(cursor.getInt(cursor.getColumnIndex(Constants.APPS.NEEDS_UPDATE)) == 1);
                    app.setSourceCode(cursor.getString(cursor.getColumnIndex(Constants.APPS.SOURCE_CODE)));
                    apps.add(app);
                    favourites.remove(id);
                }
                app.getAppBuilds().add(createAppBuildFromCursor(cursor));
            }
        }

        apps.addAll(favourites.values());

        return apps;
    }

    public de.storchp.fdroidbuildstatus.model.App loadAppBuilds(final String id, final Set<BuildRunType> buildRunTypes) {
        if (id == null) {
            return null;
        }
        de.storchp.fdroidbuildstatus.model.App app = null;
        final List<String> selectionArgs = new ArrayList<>();
        selectionArgs.add(id);
        selectionArgs.addAll(Arrays.asList(toArray(buildRunTypes)));

        try (final Cursor cursor = db.rawQuery("SELECT b." + Constants.BUILDS.BUILD_RUN_TYPE
                + ", b." + Constants.BUILDS.ID
                + ", b." + Constants.BUILDS.STATUS
                + ", b." + Constants.BUILDS.VERSION
                + ", b." + Constants.BUILDS.VERSION_CODE
                + ", b." + Constants.BUILDS.METADATA_PATH
                + ", a." + Constants.APPS.SOURCE_CODE
                + ", b." + Constants.BUILDS.LOG
                + ", a." + Constants.APPS.NAME
                + ", a." + Constants.APPS.FAVOURITE
                + ", r." + Constants.BUILD_RUNS.DATA_COMMIT_ID
                + ", a." + Constants.APPS.DISABLED
                + ", a." + Constants.APPS.NEEDS_UPDATE
                + " FROM " + DATABASE_TABLE_BUILDS + " b "
                + " LEFT JOIN " + DATABASE_TABLE_APPS + " a ON b." + Constants.BUILDS.ID + " = a." + Constants.APPS.ID
                + " LEFT JOIN " + DATABASE_TABLE_BUILD_RUNS + " r ON r." + Constants.BUILD_RUNS.BUILD_RUN_TYPE + " = b." + Constants.BUILD_RUNS.BUILD_RUN_TYPE
                + " WHERE b." + Constants.BUILDS.ID + " = ? AND b." + Constants.BUILDS.BUILD_RUN_TYPE + " IN (" + makePlaceholders(buildRunTypes.size()) + ")"
                , selectionArgs.toArray(new String[0]))) {
            while (cursor.moveToNext()) {
                if (app == null) {
                    app = new de.storchp.fdroidbuildstatus.model.App(cursor.getString(cursor.getColumnIndex(Constants.BUILDS.ID)), cursor.getString(cursor.getColumnIndex(Constants.APPS.NAME)));
                    app.setFavourite(cursor.getInt(cursor.getColumnIndex(Constants.APPS.FAVOURITE)) == 1);
                    app.setDisabled(cursor.getInt(cursor.getColumnIndex(Constants.APPS.DISABLED)) == 1);
                    app.setNeedsUpdate(cursor.getInt(cursor.getColumnIndex(Constants.APPS.NEEDS_UPDATE)) == 1);
                    app.setSourceCode(cursor.getString(cursor.getColumnIndex(Constants.APPS.SOURCE_CODE)));
                }

                app.getAppBuilds().add(createAppBuildFromCursor(cursor));
            }
        }
        return app;
    }

    private String[] toArray(final Set<BuildRunType> buildRunTypes) {
        final String[] array = new String[buildRunTypes.size()];
        int i = 0;
        for (final BuildRunType buildRunType : buildRunTypes) {
            array[i] = buildRunType.name();
            i++;
        }
        return array;
    }

    private String makePlaceholders(final int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            final StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

    @NonNull
    private AppBuild createAppBuildFromCursor(final Cursor cursor) {
        final AppBuild build = new AppBuild();
        build.setStatus(BuildStatus.valueOf(cursor.getString(cursor.getColumnIndex(Constants.BUILDS.STATUS))));
        build.setBuildRunType(BuildRunType.valueOf(cursor.getString(cursor.getColumnIndex(Constants.BUILDS.BUILD_RUN_TYPE))));
        build.setVersion(cursor.getString(cursor.getColumnIndex(Constants.BUILDS.VERSION)));
        build.setVersionCode(cursor.getString(cursor.getColumnIndex(Constants.BUILDS.VERSION_CODE)));
        build.setMetadatapath(cursor.getString(cursor.getColumnIndex(Constants.BUILDS.METADATA_PATH)));
        build.setDataCommitId(cursor.getString(cursor.getColumnIndex(Constants.BUILD_RUNS.DATA_COMMIT_ID)));
        return build;
    }

    @NonNull
    private BuildRun createBuildRunFromCursor(final Cursor cursor) {
        final BuildRun buildRun = new BuildRun();
        buildRun.setBuildRunType(BuildRunType.valueOf(cursor.getString(cursor.getColumnIndex(Constants.BUILD_RUNS.BUILD_RUN_TYPE))));
        buildRun.setStartTimestamp(cursor.getLong(cursor.getColumnIndex(Constants.BUILD_RUNS.START)));
        buildRun.setEndTimestamp(cursor.getLong(cursor.getColumnIndex(Constants.BUILD_RUNS.END)));
        buildRun.setLastModified(new Date(cursor.getLong(cursor.getColumnIndex(Constants.BUILD_RUNS.LAST_MODIFIED))));
        buildRun.setLastUpdated(new Date(cursor.getLong(cursor.getColumnIndex(Constants.BUILD_RUNS.LAST_UPDATED))));
        buildRun.setMaxBuildTimeReached(cursor.getInt(cursor.getColumnIndex(Constants.BUILD_RUNS.MAX_BUILD_TIME_REACHED)) == 1);
        buildRun.setSubcommand(cursor.getString(cursor.getColumnIndex(Constants.BUILD_RUNS.SUBCOMMAND)));
        buildRun.setCommitId(cursor.getString(cursor.getColumnIndex(Constants.BUILD_RUNS.DATA_COMMIT_ID)));
        return buildRun;
    }

    public void updateBuildLog(final String id, final String versionCode, final BuildRunType buildRunType, final String log) {
        final ContentValues values = new ContentValues();
        values.put(Constants.BUILDS.LOG, log);
        db.update(DATABASE_TABLE_BUILDS, values,
                Constants.BUILDS.ID + " = ? AND " + Constants.BUILDS.VERSION_CODE + " = ? AND " + Constants.BUILDS.BUILD_RUN_TYPE + " = ?",
                new String[]{id, versionCode, buildRunType.name()});
    }

    public void saveUpdate(final Update update) {
        Log.i(TAG, "Save new update disabled = " + update.getDisabled().size() + ", needs update = " + update.getNeedsUpdate().size());
        db.beginTransaction();

        final ContentValues valuesDis = new ContentValues();
        valuesDis.put(Constants.APPS.DISABLED, false);
        db.update(DATABASE_TABLE_APPS, valuesDis, null, null);

        valuesDis.put(Constants.APPS.DISABLED, true);
        for (final String disabledId : update.getDisabled()) {
            final int updated = db.update(DATABASE_TABLE_APPS, valuesDis, Constants.APPS.ID + " = ?", new String[]{disabledId});
            if (updated == 0) {
                final ContentValues values = new ContentValues();
                values.put(Constants.APPS.ID, disabledId);
                values.put(Constants.APPS.DISABLED, true);
                db.insert(DATABASE_TABLE_APPS, null, values);
            }
        }

        final ContentValues valuesUpd = new ContentValues();
        valuesUpd.put(Constants.APPS.NEEDS_UPDATE, false);
        db.update(DATABASE_TABLE_APPS, valuesUpd, null, null);

        valuesUpd.put(Constants.APPS.NEEDS_UPDATE, true);
        for (final String needsUpdateId : update.getNeedsUpdate()) {
            final int updated = db.update(DATABASE_TABLE_APPS, valuesUpd, Constants.APPS.ID + " = ?", new String[]{needsUpdateId});
            if (updated == 0) {
                final ContentValues values = new ContentValues();
                values.put(Constants.APPS.ID, needsUpdateId);
                values.put(Constants.APPS.NEEDS_UPDATE, true);
                db.insert(DATABASE_TABLE_APPS, null, values);
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public String getBuildLog(final String id, final String versionCode, final BuildRunType buildRunType) {
        try (final Cursor cursor = db.query(DATABASE_TABLE_BUILDS,
                new String[]{Constants.BUILDS.LOG}, Constants.BUILDS.ID + " = ? AND " + Constants.BUILDS.VERSION_CODE + " = ? AND " + Constants.BUILDS.BUILD_RUN_TYPE + " = ?",
                new String[]{id, versionCode, buildRunType.name()}, null, null, null)) {
            if (cursor.moveToFirst()) {
                return cursor.getString(cursor.getColumnIndex(Constants.BUILDS.LOG));
            }
        }

        return null;
    }

    public de.storchp.fdroidbuildstatus.model.App loadApp(final String id) {
        if (id == null) {
            return null;
        }
        try (final Cursor cursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_APPS + " WHERE " + Constants.APPS.ID + " = ?", new String[]{id})) {
            if (cursor.moveToFirst()) {
                return createAppFromCursor(cursor);
            }
        }
        return null;
    }

    public Set<AppNotification> getNotificationsFor(final BuildRunType buildRunType, final long startTimestamp) {
        final Set<AppNotification> notifiedApps = new HashSet<>();
        try (final Cursor cursor = db.query(DATABASE_TABLE_NOTIFICATIONS, new String[]{Constants.NOTIFICATIONS.ID, Constants.NOTIFICATIONS.VERSION_CODE},
                Constants.NOTIFICATIONS.BUILD_RUN_TYPE + " = ? AND " + Constants.NOTIFICATIONS.START + " = ?",
                new String[]{buildRunType.name(), String.valueOf(startTimestamp)}, null, null, null)) {
            while (cursor.moveToNext()) {
                notifiedApps.add(new AppNotification(cursor.getString(cursor.getColumnIndex(Constants.NOTIFICATIONS.ID)), cursor.getString(cursor.getColumnIndex(Constants.NOTIFICATIONS.VERSION_CODE))));
            }
        }

        return notifiedApps;
    }

    public void saveNotifications(final BuildRunType buildRunType, final long startTimestamp, final Set<AppNotification> notifiedApps) {
        db.beginTransaction();

        db.delete(DATABASE_TABLE_NOTIFICATIONS, Constants.NOTIFICATIONS.BUILD_RUN_TYPE + " = ?", new String[]{buildRunType.name()});
        for (final AppNotification appNotification: notifiedApps) {
            final ContentValues values = new ContentValues();
            values.put(Constants.NOTIFICATIONS.BUILD_RUN_TYPE, buildRunType.name());
            values.put(Constants.NOTIFICATIONS.START, startTimestamp);
            values.put(Constants.NOTIFICATIONS.ID, appNotification.getId());
            values.put(Constants.NOTIFICATIONS.VERSION_CODE, appNotification.getVersionCode());
            db.insert(DATABASE_TABLE_NOTIFICATIONS, null, values);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public List<App> findApps(final String search) {
        if (search == null) {
            return null;
        }
        final List<App> apps = new ArrayList<>();
        try (final Cursor cursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_APPS + " WHERE " + Constants.APPS.ID + " = ? OR " + Constants.APPS.NAME + " LIKE ?", new String[]{search, search + "%"})) {
            while (cursor.moveToNext()) {
                apps.add(createAppFromCursor(cursor));
            }
        }
        return apps;
    }

    static class DbOpenHelper extends SQLiteOpenHelper {

        DbOpenHelper(final Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(final SQLiteDatabase db) {
            Log.i(TAG, "Creating database");
            db.execSQL(CREATE_STATEMENT_APPS);
            db.execSQL(CREATE_STATEMENT_BUILD_RUNS);
            db.execSQL(CREATE_STATEMENT_BUILDS);
            db.execSQL(CREATE_STATEMENT_NOTIFICATIONS);
            Log.i(TAG, "Database structure created.");
        }

        @Override
        public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
            Log.w(TAG, "Upgrade database from version" + oldVersion + " to " + newVersion);
            db.beginTransaction();

            if (oldVersion < 2) {
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_APPS + " ADD COLUMN " + Constants.APPS.NAME + " TEXT");
            }
            if (oldVersion < 3) {
                db.execSQL(CREATE_STATEMENT_BUILD_RUNS);
                db.execSQL(CREATE_STATEMENT_BUILDS);
            }
            if (oldVersion == 3) {
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_BUILD_RUNS + " ADD COLUMN " + Constants.BUILD_RUNS.MAX_BUILD_TIME_REACHED + " INTEGER");
            }
            if (oldVersion >= 3 && oldVersion < 5) {
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_BUILD_RUNS + " ADD COLUMN " + Constants.BUILD_RUNS.SUBCOMMAND + " TEXT");
            }
            if (oldVersion >= 3 && oldVersion < 6) {
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_BUILD_RUNS + " ADD COLUMN " + Constants.BUILD_RUNS.DATA_COMMIT_ID + " TEXT");
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_BUILDS + " ADD COLUMN " + Constants.BUILDS.METADATA_PATH + " TEXT");
            }
            if (oldVersion >= 3 && oldVersion < 7) {
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_BUILDS + " ADD COLUMN sourceCode TEXT");
            }
            if (oldVersion < 9) {
                if (!isTableExists(db, DATABASE_TABLE_DISABLED)) {
                    db.execSQL(CREATE_STATEMENT_DISABLED);
                }
                if (!isTableExists(db, DATABASE_TABLE_NEEDS_UPDATE)) {
                    db.execSQL(CREATE_STATEMENT_NEEDS_UPDATE);
                }
            }
            if (oldVersion < 10) {
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_APPS + " ADD COLUMN " + Constants.APPS.DISABLED + " INTEGER");
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_APPS + " ADD COLUMN " + Constants.APPS.NEEDS_UPDATE + " INTEGER");
                db.execSQL("UPDATE " + DATABASE_TABLE_APPS + " SET " + Constants.APPS.DISABLED + " = 1 WHERE " + Constants.APPS.ID + " IN (SELECT " + Constants.DISABLED.ID + " FROM " + DATABASE_TABLE_DISABLED + ")");
                db.execSQL("UPDATE " + DATABASE_TABLE_APPS + " SET " + Constants.APPS.NEEDS_UPDATE + " = 1 WHERE " + Constants.APPS.ID + " IN (SELECT " + Constants.NEEDS_UPDATE.ID + " FROM " + DATABASE_TABLE_NEEDS_UPDATE + ")");
                if (isTableExists(db, DATABASE_TABLE_DISABLED)) {
                    db.execSQL("DROP TABLE " + DATABASE_TABLE_DISABLED);
                }
                if (isTableExists(db, DATABASE_TABLE_NEEDS_UPDATE)) {
                    db.execSQL("DROP TABLE " + DATABASE_TABLE_NEEDS_UPDATE);
                }
            }
            if (oldVersion < 11) {
                db.execSQL(CREATE_STATEMENT_NOTIFICATIONS);
            }
            if (oldVersion < 12) {
                db.execSQL("ALTER TABLE " + DATABASE_TABLE_APPS + " ADD COLUMN " + Constants.APPS.SOURCE_CODE + " TEXT");
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        }

        public boolean isTableExists(final SQLiteDatabase db, final String tableName) {
            try (final Cursor cursor = db.rawQuery("SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name = ?", new String[]{tableName})) {
                if(cursor!=null) {
                    return cursor.getCount() > 0;
                }
                return false;
            }
        }

    }

}
