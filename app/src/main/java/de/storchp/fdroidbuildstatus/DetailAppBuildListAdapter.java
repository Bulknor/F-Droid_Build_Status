package de.storchp.fdroidbuildstatus;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.storchp.fdroidbuildstatus.databinding.ItemDetailAppbuildBinding;
import de.storchp.fdroidbuildstatus.model.AppBuild;
import de.storchp.fdroidbuildstatus.model.BuildRunType;
import de.storchp.fdroidbuildstatus.utils.DrawableUtils;

public class DetailAppBuildListAdapter extends BaseAdapter {
    private final Activity context;
    private final List<AppBuild> appBuilds;

    public DetailAppBuildListAdapter(final Activity context, final Set<AppBuild> appBuilds) {
        super();
        this.appBuilds = new ArrayList<>(appBuilds);
        Collections.sort(this.appBuilds);
        this.context = context;
    }

    public AppBuild getItemForPosition(final int position) {
        if (appBuilds == null) {
            return null;
        }

        return appBuilds.get(position);
    }

    public int getCount() {
        return appBuilds != null ? appBuilds.size() : 0;
    }

    public Object getItem(final int position) {
        return position;
    }

    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, @NonNull final ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        final ItemDetailAppbuildBinding binding;
        if (rowView == null) {
            final LayoutInflater inflater = context.getLayoutInflater();
            binding = ItemDetailAppbuildBinding.inflate(inflater, parent, false);
            rowView = binding.getRoot();
            rowView.setTag(binding);
        } else {
            binding = (ItemDetailAppbuildBinding) rowView.getTag();
        }

        final AppBuild item = appBuilds.get(position);
        binding.version.setText(item.getFullVersion(context));
        DrawableUtils.setIconWithTint(context, binding.statusIcon, item.getStatus().getIconRes(), binding.version.getCurrentTextColor());
        if (item.getBuildRunType().isUpdateable()) {
            DrawableUtils.setIconWithTint(context, binding.buildRunType, item.getBuildRunType().getIconRes(), binding.version.getCurrentTextColor());
        } else {
            binding.buildRunType.setImageDrawable(null);
        }

        return rowView;
    }

    public int findPosBy(final String versionCode, final BuildRunType buildRunType) {
        for (int i = 0; i < appBuilds.size(); i++) {
            final AppBuild appBuild = appBuilds.get(i);
            if (appBuild.getVersionCode().equals(versionCode)
                && appBuild.getBuildRunType() == buildRunType) {
                return i;
            }
        }
        return -1;
    }

}