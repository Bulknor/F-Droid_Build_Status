package de.storchp.fdroidbuildstatus;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.storchp.fdroidbuildstatus.databinding.ItemMainAppBinding;
import de.storchp.fdroidbuildstatus.databinding.ItemMainAppbuildBinding;
import de.storchp.fdroidbuildstatus.model.App;
import de.storchp.fdroidbuildstatus.model.AppBuild;
import de.storchp.fdroidbuildstatus.model.BuildRunType;
import de.storchp.fdroidbuildstatus.utils.DrawableUtils;
import de.storchp.fdroidbuildstatus.utils.PreferenceUtils;

public class MainAppListAdapter extends BaseAdapter implements Filterable {
    private final Activity context;
    private final Object lock = new Object();
    private final List<App> allAppList;
    private final DbAdapter dbAdapter;
    private List<App> appList;
    private ArrayFilter filter;

    public MainAppListAdapter(final MainActivity context, final DbAdapter dbAdapter, final Set<BuildRunType> buildRunTypes) {
        super();
        this.allAppList = dbAdapter.loadAppBuilds(buildRunTypes, PreferenceUtils.isShowFavouritesWithoutBuildStatus(context));
        this.appList = new ArrayList<>(allAppList);
        this.context = context;
        this.dbAdapter = dbAdapter;
        sort();
    }

    public App getItemForPosition(final int position) {
        if (appList == null) {
            return null;
        }

        return appList.get(position);
    }

    public int getCount() {
        return appList != null ? appList.size() : 0;
    }

    public Object getItem(final int position) {
        return position;
    }

    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, @NonNull final ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        final ItemMainAppBinding binding;
        final LayoutInflater inflater = context.getLayoutInflater();
        if (rowView == null) {
            binding = ItemMainAppBinding.inflate(inflater, parent, false);
            rowView = binding.getRoot();
            rowView.setTag(binding);
        } else {
            binding = (ItemMainAppBinding) rowView.getTag();
        }

        final App item = appList.get(position);
        binding.appName.setText(item.getName());
        binding.appId.setText(item.getId());
        DrawableUtils.setIconWithTint(context, binding.disabledIcon, R.drawable.ic_disabled_by_default_black_24dp, binding.appName.getCurrentTextColor());
        DrawableUtils.setIconWithTint(context, binding.needsUpdateIcon, R.drawable.ic_upgrade_black_24dp, binding.appName.getCurrentTextColor());
        DrawableUtils.setIconWithTint(context, binding.favouriteIcon, item.getFavouriteIcon(), binding.appName.getCurrentTextColor());
        binding.disabledIcon.setVisibility(item.isDisabled() ? View.VISIBLE : View.GONE);
        binding.needsUpdateIcon.setVisibility(item.isNeedsUpdate() ? View.VISIBLE : View.GONE);

        binding.appBuilds.removeAllViews();
        final ArrayList<AppBuild> appBuilds = new ArrayList<>(item.getAppBuilds());
        Collections.sort(appBuilds);
        for (final AppBuild build : appBuilds) {
            @NonNull final ItemMainAppbuildBinding bindingBuild = ItemMainAppbuildBinding.inflate(inflater, parent, false);
            bindingBuild.version.setText(build.getFullVersion(context));
            if (build.getBuildRunType().isUpdateable()) {
                DrawableUtils.setIconWithTint(context, bindingBuild.buildRunType, build.getBuildRunType().getIconRes(), bindingBuild.version.getCurrentTextColor());
            } else {
                bindingBuild.buildRunType.setImageDrawable(null);
            }
            DrawableUtils.setIconWithTint(context, bindingBuild.statusIcon, build.getStatus().getIconRes(), bindingBuild.version.getCurrentTextColor());
            binding.appBuilds.addView(bindingBuild.getRoot());
        }

        return rowView;
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new ArrayFilter();
        }
        return filter;
    }

    /**
     * An array filters constrains the content of the array adapter with a prefix. Each
     * item that does not start with the supplied prefix is removed from the list.
     */
    private class ArrayFilter extends Filter {
        @Override
        protected FilterResults performFiltering(final CharSequence search) {
            final FilterResults results = new FilterResults();

            if (search == null || search.length() == 0) {
                synchronized (lock) {
                    final List<App> list = new ArrayList<>(allAppList);
                    results.values = list;
                    results.count = list.size();
                }
            } else {
                final String searchString = search.toString().toLowerCase();
                final List<App> newValues = new ArrayList<>(allAppList.size());

                for (final App item : allAppList) {
                    if ((item.getName() != null && item.getName().toLowerCase().contains(searchString))
                        || (item.getId() != null && item.getId().toLowerCase().contains(searchString))) {
                        newValues.add(item);
                    }
                }

                if (newValues.isEmpty()) {
                    final List<App> apps = dbAdapter.findApps(search.toString());
                    if (apps.isEmpty()) {
                        newValues.add(new App(search.toString(), context.getString(R.string.not_found_build_item_name)));
                    } else {
                        newValues.addAll(apps);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            //noinspection unchecked
            appList = (List<App>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }

    @Override
    public void notifyDataSetChanged() {
        sort();
        super.notifyDataSetChanged();
    }

    /**
     * Sort favourites over running build over failed build
     */
    private void sort() {
        Collections.sort(appList, (o1, o2) -> {
            final boolean fav1 = o1.isFavourite();
            final boolean fav2 = o2.isFavourite();

            if (fav1 && !fav2) {
                return -1;
            } else if (fav2 && !fav1) {
                return 1;
            }

            return o1.getId().compareTo(o2.getId());
        });
    }

}