package de.storchp.fdroidbuildstatus;

public interface Constants {

    String FDROIDDATA_BLOB_URI = "https://gitlab.com/fdroid/fdroiddata/-/blob/";
    String FDROID_PACKAGES_URI = "https://f-droid.org/packages/";
    String COUNTER = "COUNTER";

    interface TIME {
        long ONE_HOUR = 1000 * 60 * 60;
        long TWO_HOURS = 1000 * 60 * 60 * 2;
        long ONE_DAY = 1000 * 60 * 60 * 24;
    }

    interface APPS {
        String ID = "id";
        String NAME = "name";
        String FAVOURITE = "favourite";
        String DISABLED = "disabled";
        String NEEDS_UPDATE = "needs_update";
        String SOURCE_CODE = "sourceCode";
    }

    interface BUILDS {
        String BUILD_RUN_TYPE = "buildRunType";
        String ID = "id";
        String VERSION = "version";
        String VERSION_CODE = "versionCode";
        String STATUS = "status";
        String LOG = "error"; // was previous only error log
        String METADATA_PATH = "metadatapath";
    }

    interface BUILD_RUNS {
        String BUILD_RUN_TYPE = "buildRunType";
        String START = "start";
        String END = "end";
        String LAST_MODIFIED = "lastModified";
        String LAST_UPDATED = "lastUpdated";
        String MAX_BUILD_TIME_REACHED = "maxBuildTimeReached";
        String SUBCOMMAND = "subcommand";
        String DATA_COMMIT_ID = "commitId";
    }

    interface DISABLED {
        String ID = "id";
    }

    interface NEEDS_UPDATE {
        String ID = "id";
    }

    interface NOTIFICATIONS {
        String BUILD_RUN_TYPE = "buildRunType";
        String START = "start";
        String ID = "id";
        String VERSION_CODE = "versionCode";
    }

}
